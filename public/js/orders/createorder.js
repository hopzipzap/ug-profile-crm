var coef;
var i = 1;
var b = 0;
var miscCount = 0;
var miscRow = new Array();
var necondCount = 0;
var necondRow = new Array();
var pipeCount = 0;
var pipeRow = new Array();
var bi = 0;
var cost;
let productRow = [0];
var fcost = 0;
var fcostmisc = 0;
var fcostpipe = 0;
var fcostnecond = 0;
var fsqrr = 0;
var flen = 0;
var fcount = 0;

function changeType() {
    var a = document.getElementById('wavetype');
    if (a.value == "С-8") coef = 1.2;
    if (a.value == "С-20") coef = 1.14;
    if (a.value == "С-21") coef = 1.051;
    if (a.value == "НС-35") coef = 1.09;
    if (a.value == "МП-20") coef = 1.15;
    if (a.value == "Н-60") coef = 0.902;
    if (a.value == "С-8 АССИМ") coef = 1.18;
    if (a.value == "ГЛАДКИЙ ЛИСТ") coef = 1.25;
    if (a.value == "С.МОНТЕРРЕЙ") coef = 1.18;
    if (a.value == "ДРУГОЕ") orderOther();
    b = i - 1;
    while (b >= 0) {
        if (document.getElementById('product' + b) != null)
            calculateSqr('a' + b);
        b = b - 1;
    }
}

function orderOther() {
    let color = document.getElementById('mcolor');
    let thick = document.getElementById('thickness');
    let fthick = document.getElementById('fthickness');
    let list = document.getElementById('product0');
    let cost1 = document.getElementById('inputcost');
    list.remove();
    productRow = [];
    cost1.setAttribute('disabled', 'disabled');
    cost1.setAttribute('required', 'false');
    color.setAttribute('disabled', 'disabled');
    thick.setAttribute('disabled', 'disabled');
    fthick.setAttribute('disabled', 'disabled');
    color.setAttribute('required', 'false');
    thick.setAttribute('required', 'false');
    fthick.setAttribute('required', 'false');
}

function inputCost() {
    cost = document.getElementById('inputcost').value;

    b = i - 1;
    while (b >= 0) {
        if (document.getElementById('product' + b) != null) {
            calculateCost('a' + b);
        }
        b = b - 1;
    }
}

function fcostMisc() {
    fcostmisc = 0;
    var j = 0;
    var tcost = 0;
    while (j < miscRow.length) {
        tcost = Number(document.getElementById('fcostM' + miscRow[j]).value);
        fcostmisc = fcostmisc + tcost;
        j++;
    }
    finalCost();
}

function fcostPipe() {
    fcostpipe = 0;
    var j = 0;
    var tcost = 0;
    while (j < pipeRow.length) {
        tcost = Number(document.getElementById('pipefc' + pipeRow[j]).value);
        fcostpipe = fcostpipe + tcost;
        j++;
    }
    finalCost();
}

function fcostNecond() {
    fcostnecond = 0;
    var j = 0;
    var tcost = 0;
    while (j < necondRow.length) {
        tcost = Number(document.getElementById('necondfc' + necondRow[j]).value);
        fcostnecond = fcostnecond + tcost;
        j++;
    }
    finalCost();
}

function finalCost() {
    fcost = 0;
    var tcost = 0;
    var k = 0;

    while (k < productRow.length) {
        tcost = Number(document.getElementById('cost' + productRow[k]).value);
        fcost = fcost + tcost;
        k++;
    }
    fcost = fcost + fcostmisc + fcostpipe + fcostnecond;
    document.getElementById('finalcost').value = Number((fcost).toFixed(3));
}

function calculateMiscCost(fid) {
    var x = fid.match(/\d+/g);
    var cost = document.getElementById('costM' + x).value;
    var count = document.getElementById('inputMcount' + x).value;

    if (cost && count) {
        var fcost = Number(cost * count);
        document.getElementById('fcostM' + x).value = Number(fcost);
    }
    fcostMisc();
}

function calculateCost(fid) {
    var x = fid.match(/\d+/g);
    var sqr = document.getElementById('sqrr' + x).value;

    if (sqr) {
        var tcost = (sqr * cost).toFixed(3);
        document.getElementById('cost' + x).value = Number(tcost);
    }
    finalCost();
}

function calculateSqr(fid) {
    var x = fid.match(/\d+/g);
    var inputOne = document.getElementById('inputlen' + x).value;
    var inputTwo = document.getElementById('inputcount' + x).value;

    if (inputTwo && inputOne && coef) {
        document.getElementById("sqrr" + x).value = Number((inputOne * inputTwo * coef).toFixed(3));
    }
    finalSqr();
}

function deleteMisc(fid) {
    var x = fid.match(/\d+/g);
    var l = 0;

    if (miscRow.length == 1) {
        document.getElementById('misc' + x).hidden = true;
    } else {
        document.getElementById('misc' + x).remove();
        while (l < miscRow.length) {
            if (miscRow[l] == x)
                miscRow.splice(l, 1);
            l++;
        }
    }
    fcostMisc();
    document.getElementById('rowmisccount').value = miscRow.length;
    document.getElementById('rowmiscids').value = miscRow;
}

function deletePipe(fid) {
    var x = fid.match(/\d+/g);
    var l = 0;

    if (pipeRow.length == 1) {
        document.getElementById('pipe' + x).hidden = true;
    } else {
        document.getElementById('pipe' + x).remove();
        while (l < pipeRow.length) {
            if (pipeRow[l] == x)
                pipeRow.splice(l, 1);
            l++;
        }
    }
    fcostPipe();
    document.getElementById('rowpipecount').value = pipeRow.length;
    document.getElementById('rowpipeids').value = pipeRow;
}

function deleteNecond(fid) {
    var x = fid.match(/\d+/g);
    var l = 0;

    if (necondRow.length == 1) {
        document.getElementById('necond' + x).hidden = true;
    } else {
        document.getElementById('necond' + x).remove();
        while (l < necondRow.length) {
            if (necondRow[l] == x)
                necondRow.splice(l, 1);
            l++;
        }
    }
    fcostNecond();
    document.getElementById('rownecondcount').value = necondRow.length;
    document.getElementById('rownecondids').value = necondRow;
}


function deleteProduct(fid) {
    var x = fid.match(/\d+/g);
    var l = 0;

    if (productRow.length != 1) {
        document.getElementById("product" + x).remove();
        while (l < productRow.length) {
            if (productRow[l] == x)
                productRow.splice(l, 1);
            l++;
        }
        finalCost();
        finalLen();
        finalCount();
        finalSqr();
        document.getElementById('rowcount').value = productRow.length;
        document.getElementById('rowids').value = productRow;
    }
}

function finalSqr() {
    fsqrr = 0;
    var tsqr = 0;
    var k = 0;
    while (k < productRow.length) {
        tsqr = Number(document.getElementById('sqrr' + productRow[k]).value);
        fsqrr = tsqr + fsqrr;
        k++;
    }
    document.getElementById('finalsqr').value = Number(fsqrr.toFixed(3));
}

function finalLen() {
    flen = 0;
    var tlen = 0;
    var k = 0;
    while (k < productRow.length) {
        tlen = Number(document.getElementById('inputlen' + productRow[k]).value) * Number(document.getElementById('inputcount' + productRow[k]).value);
        flen = flen + tlen;
        k++;
    }
    document.getElementById('finallen').value = Number(flen);
}

function finalCount() {
    fcount = 0;
    var tcount = 0;
    var k = 0;
    while (k < productRow.length) {
        tcount = Number(document.getElementById('inputcount' + productRow[k]).value);
        fcount = tcount + fcount;
        k++;
    }
    document.getElementById('finalcount').value = Number(fcount);
}

function addMisc() {
    if (miscRow[0] && (document.getElementById('misc' + miscRow[0]).hidden == true)) {
        document.getElementById('misc' + miscRow[0]).hidden = false;
        miscCount++;
        return;
    }
    if (miscCount == 0) {
        document.getElementById('misc0').hidden = false;
        miscRow.push(miscCount);
        miscCount++;
    } else {
        var div = document.getElementById('misc' + miscRow[0]),
            clone = div.cloneNode(true);
        clone.id = 'misc' + miscCount;
        clone.querySelector('.inputMisc').id = 'inputMisc' + miscCount;
        clone.querySelector('.inputMisc').name = 'inputMisc' + miscCount;
        clone.querySelector('.inputMisc').value = '';
        clone.querySelector('.inputMcount').id = 'inputMcount' + miscCount;
        clone.querySelector('.inputMcount').name = 'inputMcount' + miscCount;
        clone.querySelector('.inputMcount').value = '';
        clone.querySelector('.costM').id = 'costM' + miscCount;
        clone.querySelector('.costM').name = 'costM' + miscCount;
        clone.querySelector('.costM').value = '';
        clone.querySelector('.fcostM').id = 'fcostM' + miscCount;
        clone.querySelector('.fcostM').name = 'fcostM' + miscCount;
        clone.querySelector('.fcostM').value = '';
        clone.querySelector('.deleteButton').id = "deleteMisc" + miscCount;
        document.getElementById('misc').appendChild(clone);
        miscRow.push(miscCount);
        miscCount++;

    }
    document.getElementById('rowmisccount').value = miscRow.length;
    document.getElementById('rowmiscids').value = miscRow;
}

function addNecond() {
    if (necondRow[0] && (document.getElementById('necond' + necondRow[0]).hidden == true)) {
        document.getElementById('necond' + necondRow[0]).hidden = false;
        necondCount++;
        return;
    }
    if (necondCount == 0) {
        document.getElementById('necond0').hidden = false;
        necondRow.push(necondCount);
        necondCount++;
    } else {
        var div = document.getElementById('necond' + necondRow[0]),
            clone = div.cloneNode(true);
        clone.id = 'necond' + necondCount;
        clone.querySelector('.necondlist').setAttribute('list', 'select-necond' + necondCount);
        clone.querySelector('.necondlist').id = 'input-necond' + necondCount;
        clone.querySelector('.necondlist').name = 'input-necond' + necondCount;
        clone.querySelector('.necondlist').value = '';
        clone.querySelector('.select-necond').id = 'select-necond' + necondCount;
        clone.querySelector('.select-necond').name = 'select-necond' + necondCount;
        clone.querySelector('.select-necond').value = '';
        clone.querySelector('.necondpm').id = 'necondpm' + necondCount;
        clone.querySelector('.necondpm').name = 'necondpm' + necondCount;
        clone.querySelector('.necondpm').value = '';
        clone.querySelector('.necondcpm').id = 'necondcpm' + necondCount;
        clone.querySelector('.necondcpm').name = 'necondcpm' + necondCount;
        clone.querySelector('.necondcpm').value = '';
        clone.querySelector('.necondfc').id = 'necondfc' + necondCount;
        clone.querySelector('.necondfc').name = 'necondfc' + necondCount;
        clone.querySelector('.necondfc').value = '';
        clone.querySelector('.deleteButton').id = "deleteNecond" + necondCount;
        document.getElementById('necond').appendChild(clone);
        necondRow.push(necondCount);
        necondCount++;
    }
    document.getElementById('rownecondcount').value = necondRow.length;
    document.getElementById('rownecondids').value = necondRow;
}

function addPipe() {
    if (pipeRow[0] && (document.getElementById('pipe' + pipeRow[0]).hidden == true)) {
        document.getElementById('pipe' + pipeRow[0]).hidden = false;
        pipeCount++;
        return;
    }
    if (pipeCount == 0) {
        document.getElementById('pipe0').hidden = false;
        pipeRow.push(pipeCount);
        pipeCount++;
    } else {
        var div = document.getElementById('pipe' + pipeRow[0]),
            clone = div.cloneNode(true);

        clone.id = 'pipe' + pipeCount;
        clone.querySelector('.pipelist').setAttribute('list', 'select-pipe' + pipeCount);
        clone.querySelector('.pipelist').id = 'input-pipe' + pipeCount;
        clone.querySelector('.pipelist').name = 'input-pipe' + pipeCount;
        clone.querySelector('.pipelist').value = '';
        clone.querySelector('.select-pipe').id = 'select-pipe' + pipeCount;
        clone.querySelector('.select-pipe').name = 'select-pipe' + pipeCount;
        clone.querySelector('.select-pipe').value = '';
        clone.querySelector('.input-select.pm').id = 'pipepm' + pipeCount;
        clone.querySelector('.input-select.pm').name = 'pipepm' + pipeCount;
        clone.querySelector('.input-select.pm').value = '';
        clone.querySelector('.input-select.costpm').id = 'pipecpm' + pipeCount;
        clone.querySelector('.input-select.costpm').name = 'pipecpm' + pipeCount;
        clone.querySelector('.input-select.costpm').value = '';
        clone.querySelector('.input-select.costfinal').id = 'pipefc' + pipeCount;
        clone.querySelector('.input-select.costfinal').name = 'pipefc' + pipeCount;
        clone.querySelector('.input-select.costfinal').value = '';
        clone.querySelector('.deleteButton').id = "deletePipe" + pipeCount;
        document.getElementById('pipe').appendChild(clone);
        pipeRow.push(pipeCount);
        pipeCount++;
    }
    document.getElementById('rowpipecount').value = pipeRow.length;
    document.getElementById('rowpipeids').value = pipeRow;
}

function addProduct() {
    var div = document.getElementById('product' + productRow[0]),
        clone = div.cloneNode(true);

    bi = bi + 1;
    clone.id = "product" + i;
    clone.querySelector('.deleteButton').id = "deleteButton" + bi;
    clone.querySelector('.inputlen').id = "inputlen" + i;
    clone.querySelector('.inputcount').id = "inputcount" + i;
    clone.querySelector('.sqrr').id = "sqrr" + i;
    clone.querySelector('.cost').id = "cost" + i;
    clone.querySelector('.inputlen').name = "inputlen" + i;
    clone.querySelector('.inputcount').name = "inputcount" + i;
    clone.querySelector('.sqrr').name = "sqrr" + i;
    clone.querySelector('.cost').name = "cost" + i;
    clone.querySelector('.inputlen').value = '';
    clone.querySelector('.inputcount').value = '';
    clone.querySelector('.sqrr').value = '';
    clone.querySelector('.cost').value = '';
    productRow.push(i);
    i = i + 1;
    document.getElementById('profnastil').appendChild(clone);
    document.getElementById('rowcount').value = productRow.length;
    document.getElementById('rowids').value = productRow;
}

function calculatePipe(fid) {
    var x = fid.match(/\d+/g);
    var fcostfield = document.getElementById('pipefc' + x);
    var costfield = document.getElementById('pipecpm' + x);
    var pmfield = document.getElementById('pipepm' + x);
    fcostfield.value = Number((Number(costfield.value) * Number(pmfield.value)).toFixed(3));
    fcostPipe();
}

function calculateNecond(fid) {
    var x = fid.match(/\d+/g);
    var pmfield = Number(document.getElementById('necondpm' + x).value);
    var costfield = Number(document.getElementById('necondcpm' + x).value);
    var fcostfield = (document.getElementById('necondfc' + x));
    fcostfield.value = Number((pmfield * costfield).toFixed(3));
    fcostNecond();
}

function checkSubmit() {
    if ((miscRow.length + pipeRow.length + necondRow.length + productRow.length) > 0) {
        return true;
    } else {
        alert('Пустой заказ');
        return false;
    }
}

$(function() {
    $('select').selectpicker();
});