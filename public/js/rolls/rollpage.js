console.log(role);
if (role != 'Администратор') {
    if (role != 'Менеджер по отгрузкам') {
        if (role != 'Менеджер производства') {
            if (role != 'Кладовщик') {
                document.getElementById('discardButton').remove();
                document.getElementById('tag-div').remove();
                document.getElementById('doc-div').remove();
            }
        }
    }
}

loadRoll();

role = document.getElementById('session-role').innerHTML;


function loadRoll() {
    var req = new XMLHttpRequest();
    var url = '/getrolldata';
    req.open('POST', url, true);
    req.addEventListener('load', onRollLoad);
    req.addEventListener('error', onError);
    req.send();

}

function onRollLoad() {
    var response = this.responseText;
    var roll = JSON.parse(response);
    var date = new Date(roll.acceptdate).addHours(3).toLocaleString().slice(0, 10)
    var disdate = new Date(roll.discarddate).addHours(3).toLocaleString().slice(0, 10)
    var passsrc = '/img/acceptuploads/' + roll.passimgname;
    var birkasrc = '/img/acceptuploads/' + roll.birkaimgname;

    if (roll.status != 0) {
        if (document.getElementById('discardButton'))
            document.getElementById('discardButton').remove();
    }
    if (document.getElementById('discardButton')) {
        document.getElementById('birka-img').src = birkasrc;
        document.getElementById('passport-img').src = passsrc;
    }
    document.getElementById('vendor').innerHTML += '     ' + roll.supply;
    document.getElementById('thick').innerHTML += '     ' + roll.thickness;
    document.getElementById('color').innerHTML += '     ' + roll.color;
    document.getElementById('pm').innerHTML += '     ' + roll.pmfull;
    document.getElementById('pmleft').innerHTML += '     ' + roll.pm;
    document.getElementById('date').innerHTML += '     ' + date;
    document.getElementById('wagon').innerHTML += '     ' + roll.wagon;
    document.getElementById('acceptor').innerHTML += '     ' + roll.prinyal;
    document.getElementById('other').innerHTML += '     ' + roll.prim;
    if (roll.discarddate != null) {
        document.getElementById('disdate').innerHTML += '  ' + disdate;
    }


}


function onError(errorStatus, errorMessage) {
    let xml = new XMLHttpRequest();
    xml.open('POST', '/clienterror', true);
    let send = { status: errorStatus, message: errorMessage };
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(send));
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}