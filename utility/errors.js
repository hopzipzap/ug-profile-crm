const fs = require('fs');

module.exports = {
    logClientErrors: function(status, message, clientName) {
        let currentdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
        let output = `Error Status:  ${status}    With message: ${message}    Date: ${currentdate}    Name: ${clientName}\n\n`;
        let line = '\n/*------------------------------------------------------------------------------------------------------------*/\n\n';
        console.log(output + line);

        fs.appendFile('ERRORS/clienterror.log', output + line, function(error) {
            if (error) throw createError(error);
        });
    },

    mysqlErrorHandler: function(err) {
        let currentdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
        let message = `Error code: ${err.code}    With message: ${err.sqlMessage}  Date: ${currentdate}\n ${err.stack}\n\n`;
        let line = '/*-------------------------------------------------------------------------------------------------------------*/\n\n';
        console.log(message + line);
        fs.appendFile('ERRORS/mysqlerror.log', message + line, function(error) {
            if (error) throw createError(error);
        });
        //throw err;
    },
};