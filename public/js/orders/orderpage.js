var req = new XMLHttpRequest();
var url = '/getposdata';
req.open('POST', url, true);
req.addEventListener('load', onLoad);
req.addEventListener('error', onError);
req.send();
let order;
let fthick = new String();
let thick1 = new String();
let miscToDelete = new Array();
let miscToChange = new Array();
let posToChange = new Array();
let posToCreate = new Array();
let posToDelete = new Array();
let miscToAdd = new Array();
let pipeToAdd = new Array();
let pipeToChange = new Array();
let pipeToDelete = new Array();
let necondToAdd = new Array();
let necondToChange = new Array();
let necondToDelete = new Array();
let m2cost = 0;
let lastid = 0;
let lastmiscid = 0;
let lastpipeid = 0;
let lastnecondid = 0;
let printflag = 0;
let printBirka = 0;
var i = 0;
var fcount = 0;
var fpm = 0;
var fsqr = 0;
var fcost = 0;

let test = "<datalist id='select-pipe0' class='select-pipe'> <option class='optpipe' value='15*15*1.5'>15*15*1.5</option><option class='optpipe' value='15*15*1.8'>15*15*1.8</option><option class='optpipe' value='20*20*1.35'>20*20*1.35</option><option class='optpipe' value='20*20*1.5'>20*20*1.5</option><option class='optpipe' value='20*20*1.8'>20*20*1.8</option><option class='optpipe' value='20*20*2'>20*20*2</option><option class='optpipe' value='30*30*1.35'>30*30*1.35</option> <option class='optpipe' value='30*30*1.5'>30*30*1.5</option><option class='optpipe' value='30*30*1.8'>30*30*1.8</option><option class='optpipe' value='30*30*2'>30*30*2</option><option class='optpipe' value='40*20*1.35'>40*20*1.35</option><option class='optpipe' value='40*20*1.5'>40*20*1.5</option><option class='optpipe' value='40*20*1.8'>40*20*1.8</option><option class='optpipe' value='40*20*2'>40*20*2</option><option class='optpipe' value='40*25*1.5'>40*25*1.5</option><option class='optpipe' value='40*25*1.8'>40*25*1.8</option><option class='optpipe' value='40*25*2'>40*25*2</option><option class='optpipe' value='40*40*1.5'>40*40*1.5</option><option class='optpipe' value='40*40*1.8'>40*40*1.8</option><option class='optpipe' value='40*40*2'>40*40*2</option><option class='optpipe' value='40*40*2.8'>40*40*2.8</option><option class='optpipe' value='40*40*3'>40*40*3</option><option class='optpipe' value='50*25*1.5'>50*25*1.5</option><option class='optpipe' value='50*25*1.8'>50*25*1.8</option> <option class='optpipe' value='50*25*2'>50*25*2</option><option class='optpipe' value='50*50*1.8'>50*50*1.8</option><option class='optpipe' value='50*50*2'>50*50*2</option><option class='optpipe' value='60*30*1.8'>60*30*1.8</option><option class='optpipe' value='60*30*2'>60*30*2</option><option class='optpipe' value='60*30*2.8'>60*30*2.8</option><option class='optpipe' value='60*30*3'>60*30*3</option><option class='optpipe' value='60*40*1.5'>60*40*1.5</option><option class='optpipe' value='60*40*1.8'>60*40*1.8</option><option class='optpipe' value='60*40*2'>60*40*2</option><option class='optpipe' value='60*40*2.8'>60*40*2.8</option><option class='optpipe' value='60*40*3'>60*40*3</option><option class='optpipe' value='60*60*1.8'>60*60*1.8</option><option class='optpipe' value='60*60*2'>60*60*2</option><option class='optpipe' value='60*60*2.8'>60*60*2.8</option><option class='optpipe' value='60*60*3'>60*60*3</option><option class='optpipe' value='80*40*1.8'>80*40*1.8</option><option class='optpipe' value='80*40*2'>80*40*2</option><option class='optpipe' value='80*40*2.8'>80*40*2.8</option><option class='optpipe' value='80*40*3'>80*40*3</option><option class='optpipe' value='80*60*2'>80*60*2</option><option class='optpipe' value='80*60*2.8'>80*60*2.8</option><option class='optpipe' value='80*60*3'>80*60*3</option><option class='optpipe' value='80*80*2'>80*80*2</option><option class='optpipe' value='80*80*2.8'>80*80*2.8</option><option class='optpipe' value='80*80*3'>80*80*3</option><option class='optpipe' value='80*80*4'>80*80*4</option><option class='optpipe' value='100*50*2.8'>100*50*2.8</option><option class='optpipe' value='100*50*3'>100*50*3</option><option class='optpipe' value='100*50*4'>100*50*4</option><option class='optpipe' value='100*100*2.8'>100*100*2.8</option><option class='optpipe' value='100*100*3'>100*100*3</option><option class='optpipe' value='100*100*4'>100*100*4</option><option class='optpipe' value='120*120*3'>120*120*3</option><option class='optpipe' value='120*120*4'>120*120*4</option><option class='optpipe' value='140*140*4'>140*140*4</option> <option class='optpipe' value='140*140*5'>140*140*5</option></datalist>";

function onLoad() {
    var response = this.responseText;
    var pos = JSON.parse(response);
    var sqr = 0;
    var pm = 0;
    var icost = 0;
    getOrderData();
    document.getElementById('poscount').value = pos.id.length;
    if (pos.length[0]) {

        while (i < pos.id.length) {
            document.getElementById('posids').value += pos.id[i] + ',';
            var sqr = Number((pos.sqr[i] / pos.count[i]).toFixed(3));
            var pm = Number((pos.count[i] * pos.length[i]).toFixed(3));
            icost = pos.costpm[i];
            if (!icost)
                icost = Number((pos.cost[i] / pos.sqr[i]).toFixed(3));
            m2cost = icost;
            fcount += Number(pos.count[i]);
            fpm += Number(pm);
            fcost += Number(pos.cost[i]);
            fsqr += Number(pos.sqr[i]);
            var row = document.createElement("tr");
            row.id = 'mrow' + pos.id[i];
            lastid = pos.id[i];
            row.innerHTML = "<td id='row" + pos.id[i] + "'>" + (i + 1) + "</td><td id='len" + pos.id[i] + "'>" + "<input readonly name='leni" + pos.id[i] + "' class='lenirow' id='leni" + pos.id[i] + "' type='number' min='0.3' max='20' step='0.05' style='border:none; width:200px;' oninput='changeSqr(this.id)' value='" + Number(pos.length[i]) + "' > </td><td id='count" + pos.id[i] + "'>" + "<input readonly class='countirow' name='counti" + pos.id[i] + "' id='counti" + pos.id[i] + "' type='number' min='1' step='1' style='border:none; width:100px' oninput='changeSqr(this.id)' value='" + pos.count[i] + "'></td><td><input readonly name='sqr" + pos.id[i] + "' id='sqr" + pos.id[i] + "'  value='" + Number(sqr) + "'></td><td> <input readonly class='fsqrrow' name='fsqr" + pos.id[i] + "' id='fsqr" + pos.id[i] + "' value='" + Number(pos.sqr[i]) + "'></td><td name='icost" + pos.id[i] + "' id='icost" + pos.id[i] + "'>" + Number(icost) + "</td><td><input readonly class='costrow' name='cost" + pos.id[i] + "' id='cost" + pos.id[i] + "' value='" + Number(pos.cost[i]) + "'></td><td><input class='pmrow' name='pm" + pos.id[i] + "' id='pm" + pos.id[i] + "' value='" + Number(pm) + "'></td><td id='prim" + pos.id[i] + "'><div id='del" + pos.id[i] + "' class='deldiv' style='display:flex'><input style='width:100%; border:none' type='text'></div></td>";
            document.getElementById("node1").appendChild(row);
            i++;
        }
        fpm.toFixed(3);
        fcost.toFixed(3);
        fsqr.toFixed(3);
        row = document.createElement("tr");
        row.style.border = "3px solid black";
        row.style.fontWeight = "bold";
        row.id = 'finalrow';
        row.innerHTML = "<td>#</td><td>Итого</td><td><input name='fcount' id='fcount' value='" + Number(fcount) + "'></td><td></td><td><input name='fsqr' id='fsqr' value='" + Number(fsqr.toFixed(3)) + "'></td><td></td><td><input name='fcost' id='fcost' value='" + Number(fcost.toFixed(3)) + "'></td><td><input name='fpm' id='fpm' value='" + Number(fpm.toFixed(3)) + "'></td><td><input style='width:100%; border:none' type='text'>" + "</td>";
        document.getElementById("node1").appendChild(row);
    }
    getMiscData();
}

function getOrderData() {
    var xml = new XMLHttpRequest();
    xml.open('POST', '/getorder', true);
    xml.addEventListener('load', onOrderLoad);
    xml.addEventListener('error', onError);
    xml.send();

}

function onOrderLoad() {
    var res = this.responseText;
    order = JSON.parse(res);

    var sessionName = document.querySelector('#session-name').innerHTML;
    var sessionRole = document.querySelector('#session-role').innerHTML;
    let departdate;
    let d1 = new Date(order.createdate).addHours(3).toLocaleString().slice(0, 10);
    checkWhoIs(order.manager, order.id, sessionName, sessionRole, order.status, order.uniqid);
    let readydate = new Date(order.readydate).addHours(3).toLocaleString().slice(0, 10);
    thick1 = order.thick;
    fthick = order.fthick;
    document.getElementById('costpm').value = order.cost;
    if (order.wavetype == 'ДРУГОЕ') {
        document.querySelector('.product').hidden = true;
        document.querySelector('.product').style.height = '0px';
        document.getElementById('labelwave').innerHTML = ' ';
        document.querySelector('.color-field').hidden = true;
        document.querySelector('.thick-field').hidden = true;
        document.getElementById('poss').style.visibility = 'hidden';
        document.getElementById('poss').style.height = '0px';
    }
    if (order.departdate != null)
        departdate = new Date(order.departdate).addHours(3).toLocaleString().slice(0, 10);
    else
        departdate = '';
    document.getElementById('uniqid').value = order.uniqid;
    document.querySelector('.phonenumber').innerHTML = order.phonenumber;
    document.getElementById('payment').innerHTML = order.payment;
    document.getElementById('fullcost').innerHTML = Math.ceil(Number(order.finalcost)) + '&nbsp';
    document.getElementById('misc1').innerHTML = order.misc;
    document.getElementById('manager').innerHTML = order.manager;
    document.querySelector('.readydate1').value = readydate;
    document.querySelector('.departdate1').innerHTML = departdate;
    document.querySelector('.wavetype').innerHTML = order.wavetype;
    document.querySelector('.afterwave').innerHTML = " " + getSqr(order.wavetype);
    document.querySelector('.color').innerHTML = order.color;
    document.querySelector('.thick').value = "Толщина: " + order.thick;
    document.querySelector('.createdate1').innerHTML = d1;
    document.querySelector('.client').innerHTML = order.client;
}

function getMiscData() {
    var xml = new XMLHttpRequest();
    xml.open('POST', '/getmiscdata', true);
    xml.addEventListener('load', onMiscLoad);
    xml.addEventListener('error', onError);
    xml.send();
}

function onMiscLoad() {
    var res = this.responseText;
    var misc = JSON.parse(res);
    var j = 0;

    if (misc.id.length > 0) {
        var frow = document.createElement("tr");
        frow.id = 'frow';
        frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
        document.getElementById("node1").appendChild(frow);
        while (j < misc.id.length) {
            var row = document.createElement("tr");
            lastmiscid = misc.id[j];
            row.id = 'mirow' + lastmiscid;
            row.innerHTML = "<td class='miscrow' id='miscrow" + lastmiscid + "'>" + (i + 1) + "</td><td><input style='width:195px' oninput='miscChange(this.id)' required class='miscprod' readonly id=='miscprod" + lastmiscid + "' name='miscprod" + lastmiscid + "' value='" + misc.product[j] + "'></td><td><input oninput='calculateMisc(this.id)' type='number' required class='misccount' readonly name='misccount" + lastmiscid + "' id='misccount" + lastmiscid + "' value='" + misc.count[j] + "'>шт</td><td>\
                             <input readonly oninput='calculateMisc(this.id)' type='number' required class='misccost' name='misccost" + lastmiscid + "' id='misccost" + lastmiscid + "' value='" + misc.cost[j] + "'></td><td><input readonly class='miscfcost' name='miscfcost" + lastmiscid + "' id='miscfcost" + lastmiscid + "' value='" + misc.fcost[j] + "'></td><td> </td><td> </td><td><div id='delmisc" + lastmiscid + "' class='delmisc' style='display:flex'><input style='width:100%; border:none' type='text'></div></td>";
            document.getElementById("node1").appendChild(row);
            j++;
            i++;
        }
    }
    getPipeData();
}

function getPipeData() {
    var xml = new XMLHttpRequest();
    xml.open('POST', '/getpipedata', true);
    xml.addEventListener('load', onPipeLoad);
    xml.addEventListener('error', onError);
    xml.send();
}

function onPipeLoad() {
    var res = this.responseText;
    var pipe = JSON.parse(res);
    var j = 0;

    if (pipe.id.length > 0) {
        if (!document.getElementById('frow')) {
            var frow = document.createElement("tr");
            frow.id = 'frow';
            frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
            document.getElementById("node1").appendChild(frow);
        }
        while (j < pipe.id.length) {
            var row = document.createElement("tr");
            lastpipeid = pipe.id[j];
            row.id = 'pirow' + lastpipeid;
            row.innerHTML = "<td class='piperow' id='piperow" + lastpipeid + "'>" + (i + 1) + "</td><td><input style='width:195px' oninput='pipeChange(this.id)' required class='pipetype' readonly id=='pipetype" + lastpipeid + "' name='pipetype" + lastpipeid + "' value='" + pipe.type[j] + "'></td><td><input oninput='calculatePipe(this.id)' type='number' required  class='pipepm' readonly name='pipepm" + lastpipeid + "' id='pipepm" + lastpipeid + "' value='" + pipe.pm[j] + "'>м</td><td>\
                             <input readonly oninput='calculatePipe(this.id)' type='number' required class='pipecost' name='pipecost" + lastpipeid + "' id='pipecost" + lastpipeid + "' value='" + pipe.cost[j] + "'></td><td><input readonly class='pipefcost' name='pipefcost" + lastpipeid + "' id='pipefcost" + lastpipeid + "' value='" + pipe.fcost[j] + "'></td><td> </td><td> </td><td><div id='delpipe" + lastpipeid + "' class='delpipe' style='display:flex'><input style='width:100%; border:none' type='text'></div></td>";
            document.getElementById("node1").appendChild(row);
            j++;
            i++;
        }
    }
    getNecondData();
}

function getNecondData() {
    var xml = new XMLHttpRequest();
    xml.open('POST', '/getneconddata', true);
    xml.addEventListener('load', onNecondLoad);
    xml.addEventListener('error', onError);
    xml.send();
}

function onNecondLoad() {
    var res = this.responseText;
    var necond = JSON.parse(res);
    var j = 0;
    if (necond.id.length > 0) {
        if (!document.getElementById('frow')) {
            var frow = document.createElement("tr");
            frow.id = 'frow';
            frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
            document.getElementById("node1").appendChild(frow);
        }
        while (j < necond.id.length) {
            var row = document.createElement("tr");
            lastnecondid = necond.id[j];
            row.id = 'necrow' + lastnecondid;
            row.innerHTML = "<td class='necondrow' id='necondrow" + lastnecondid + "'>" + (i + 1) + "\
            </td><td><input style='width:195px' oninput='necondChange(this.id)' required class='nectype' readonly id=='nectype" + lastnecondid + "' name='nectype" + lastnecondid + "' value='" + necond.type[j] + "'></td><td><input oninput='calculateNecond(this.id)' type='number' required class='necsqr' readonly name='necsqr" + lastnecondid + "' id='necsqr" + lastnecondid + "' value='" + necond.sqr[j] + "'>м2</td><td>\
            <input readonly oninput='calculateNecond(this.id)' type='number' required class='neccost' name='neccost" + lastnecondid + "' id='neccost" + lastnecondid + "' value='" + necond.cost[j] + "'></td><td><input readonly class='necfcost' name='necfcost" + lastnecondid + "' id='necfcost" + lastnecondid + "' value='" + necond.fcost[j] + "'></td><td> </td><td> </td><td><div id='delnecond" + lastnecondid + "' class='delnecond' style='display:flex'>\
            <input style='width:100%; border:none' type='text'></div></td>";
            document.getElementById("node1").appendChild(row);
            j++;
            i++;
        }
    }
}

function checkWhoIs(manager, ordid, sessionName, sessionRole, status, uniqid) {
    if ((sessionRole == "Администратор" || sessionRole == "Менеджер производства") || sessionName == manager) {
        allowOrderChange(ordid, status, sessionRole, sessionName, uniqid);
        return true;
    } else {
        return false;
    }
}

function allowOrderChange(ordid, status, role, name, uniqid) {
    let res = new String;
    let ftv = new Array();
    /* CHANGE STATUS TO IN PRODUCTION */
    ftv[0] = "<form method='GET' action='/changestatus1/" + ordid + "' onSubmit=''><button type='submit' class='changestatus1-btn'>В производство</button></form>";
    /* CHANGE STATUS BACK TO AWAITING */
    ftv[1] = "<form method='GET' action='/changestatus0/" + ordid + "' onSubmit=''><button type='submit' class='changestatus0-btn'>Вернуть</button></form>";
    /* CHANGE STATUS TO AWAIT FROM IN PRODUCTION */
    ftv[2] = "<form method='GET' action='/cancelstatus1/" + ordid + "' onSubmit=''><button type='submit' class='cancelstatus1-btn'>Убрать из производства</button></form>";
    if (status == 5 && (role != 'Администратор'))
        ftv[2] = '';
    /* CHANGE STATUS TO CANCELED */
    ftv[3] = "<form method='GET' action='/cancelorder/" + ordid + "' onSubmit='return confirm('Вы уверены что хотите отменить заказ?');'><button type='submit' class='cancelorder-btn'>Отменить</button></form>";
    /* DELETE ORDER */
    ftv[4] = "<form method='POST' action='/deleteorder/" + ordid + "' onSubmit='return confirm('Вы уверены что хотите удалить заказ?');'><input hidden name='uniqid' value='" + uniqid + "'><button type='submit' class='deleteorder-btn'>Удалить</button></form>";
    /* ORDER READY */
    ftv[5] = "<div class='ready-form'><form style='display:inline-flex' method='POST' action='/changestatusready/" + ordid + "' onSubmit=''><input type='hidden' id='pm2' name='pm2' value=''><input placeholder='Сырье' name='rolls' id='rolls' class='inputa'><input placeholder='Использовано' name='used' id='used' class='inputa'><input placeholder='Некондиция' name='necond' id='necond' class='inputa'><button type='submit' class='readyorder-btn'>Готово</button></form></div>";
    if (status == 5 && (role != 'Администратор')) {
        if (role != 'Менеджер производства')
            ftv[5] = '';
    }
    /* SAVE CHANGES */
    ftv[6] = "<button form='changeorder' class='saveorder-btn' onclick='changeOrder()'>Изменить</button>";
    /* DEPART */
    ftv[7] = "<form method='GET' action='/changestatusdepart/" + ordid + "'><button class='saveorder-btn'>Отгрузить</button></form>";
    if (role == 'Администратор' || role == 'Менеджер производства')
        ftv[8] = "<form method='GET' action='/acceptproduction/" + ordid + "'><button class='saveorder-btn'>Подтвердить</button></form>";
    else
        ftv[8] = '';
    console.log(status);
    switch (status) {
        case 0:
            res = ftv[0] + ftv[3] + ftv[6];
            break;
        case 1:
            res = ftv[2] + ftv[8];
            break;
        case 2:
            res = ftv[7];
            break;
        case 3:
            res = ftv[1] + ftv[4];
            break;
        case 5:
            res = ftv[2] + ftv[5];
            break;
    }
    document.querySelector('.foot').innerHTML = res;
}



function addMisc() {
    i++;
    lastmiscid++;

    if (!(document.getElementById('frow'))) {
        var frow = document.createElement("tr");
        frow.id = 'frow';
        frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
        document.getElementById("node1").appendChild(frow);
    }
    let row = document.createElement('tr');
    row.id = 'row' + i;
    row.innerHTML = "<td class='miscrow" + lastmiscid + "'>" + i + "</td><td id='miscprod" + lastmiscid + "'><input type='text' name='miscprod" + lastmiscid + "' id='miscprod" + lastmiscid + "'></td><td><input type='number' oninput='calculateMisc(this.id)' name='misccount" + lastmiscid + "' id='misccount" + lastmiscid + "'></td><td><input type='number' oninput='calculateMisc(this.id)' required name='misccost" + lastmiscid + "' id='misccost" + lastmiscid + "'></td><td><input readonly class='miscfcost' name='miscfcost" + lastmiscid + "' id='miscfcost" + lastmiscid + "'></td><td></td><td></td><td></td>";
    document.getElementById('node1').appendChild(row);
    miscToAdd.push(String(lastmiscid));
    document.getElementById('misctoadd').value = miscToAdd;
}

function addPipe() {
    i++;
    lastpipeid++;
    if (!(document.getElementById('frow'))) {
        var frow = document.createElement("tr");
        frow.id = 'frow';
        frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
        document.getElementById("node1").appendChild(frow);
    }
    let row = document.createElement('tr');
    row.id = 'row' + i;
    row.innerHTML = "<td class='piperow" + lastpipeid + "'>" + i + "</td><td id='pipetype" + lastpipeid + "'><input type='text'  name='pipetype" + lastpipeid + "' id='pipetype" + lastpipeid + "'></td><td><input type='number' oninput='calculatePipe(this.id)' name='pipepm" + lastpipeid + "' id='pipepm" + lastpipeid + "'>м.</td><td><input type='number' oninput='calculatePipe(this.id)' name='pipecost" + lastpipeid + "' id='pipecost" + lastpipeid + "'></td><td><input readonly class='pipefcost' name='pipefcost" + lastpipeid + "' id='pipefcost" + lastpipeid + "'></td><td></td><td></td><td></td>";
    document.getElementById('node1').appendChild(row);
    pipeToAdd.push(String(lastpipeid));
    document.getElementById('pipetoadd').value = pipeToAdd;
}

function addNecond() {
    i++;
    lastnecondid++;
    if (!(document.getElementById('frow'))) {
        var frow = document.createElement("tr");
        frow.id = 'frow';
        frow.innerHTML = "<td> # </td><td> Продукт </td><td> Кол - во </td><td> Цена шт </td><td> Цена итого </td><td> </td><td> </td><td><input style='width:100%; border:none' type='text'>" + "</td>"
        document.getElementById("node1").appendChild(frow);
    }
    let row = document.createElement('tr');
    row.id = 'row' + i;
    row.innerHTML = "<td class='necondrow" + lastnecondid + "'>" + i + "</td><td id='nectype" + lastnecondid + "'><input type='text' name='nectype" + lastnecondid + "' id='nectype" + lastnecondid + "'></td><td><input type='number' oninput='calculateNecond(this.id)' name='necsqr" + lastnecondid + "' id='necsqr" + lastnecondid + "'>м.кв.</td><td><input type='number' oninput='calculateNecond(this.id)' name='neccost" + lastnecondid + "' id='neccost" + lastnecondid + "'></td><td><input readonly class='necfcost' name='necfcost" + lastnecondid + "' id='necfcost" + lastnecondid + "'></td><td></td><td></td><td></td>";
    document.getElementById('node1').appendChild(row);
    necondToAdd.push(String(lastnecondid));
    document.getElementById('nectoadd').value = necondToAdd;
}



function addSheet() {
    let crcount = 0;
    i++;
    lastid++;
    var row = document.createElement("tr");
    row.id = 'row' + i;
    crcount++;
    posToCreate.push(String(lastid));
    console.log('PostoChange ' + posToChange);
    console.log('PosToDelete ' + posToDelete);
    console.log('PosToCreate ' + posToCreate);
    document.getElementById('postocreate').value = posToCreate;
    document.getElementById('createdposcount').value = crcount;
    document.getElementById('posids').value += lastid + ',';
    row.innerHTML = "<td>" + i + "</td><td id='len" + lastid + "'>" + "<input  name='leni" + lastid + "' class='lenirow' id='leni" + lastid + "' type='number' min='0.3' max='20' step='0.05' style='border:none; width:100px' oninput='changeSqr(this.id)' value='' > </td><td id='count" + lastid + "'>" + "<input  class='countirow' name='counti" + lastid + "' id='counti" + lastid + "' type='number' min='1' step='1' style='border:none; width:100px' oninput='changeSqr(this.id)' value=''></td><td><input readonly name='sqr" + lastid + "' id='sqr" + lastid + "'  value=''></td><td> <input readonly class='fsqrrow' name='fsqr" + lastid + "' id='fsqr" + lastid + "' value=''></td><td name='icost" + lastid + "' id='icost" + lastid + "'>" + Number(m2cost) + "</td><td><input readonly class='costrow' name='cost" + lastid + "' id='cost" + lastid + "' value=''></td><td><input class='pmrow' name='pm" + lastid + "' id='pm" + lastid + "' value=''></td><td id='prim" + lastid + "'><div id='del" + lastid + "' class='deldiv' style='display:flex'><input style='width:100%; border:none' type='text'></div></td>";
    document.getElementById("node1").insertBefore(row, document.getElementById('finalrow'));
}

function changeOrder() {
    var k = 0;
    let save_btn = "<button form='saveform' class='saveorder-btn' >Сохранить</button>";
    let cancelChange_btn = "<button type='button' class='saveorder-btn' onclick='updatePage()'>Отменить</button>";
    let addSheet = "<button type='button' class='saveorder-btn' onclick='addSheet()'>+ лист</button>";
    let addMisc = "<button type='button' class='saveorder-btn' onclick='addMisc()'>+ другое</button>";
    let addPipe = "<button type='button' class='saveorder-btn' onclick='addPipe()'>+ труба</button>";
    let addNecond = "<button type='button' class='saveorder-btn' onclick='addNecond()'>+ неконд</button>";
    let miscprodarr = document.getElementsByClassName('miscprod');
    let misccountarr = document.getElementsByClassName('misccount');
    let misccostarr = document.getElementsByClassName('misccost');
    let pipetypearr = document.getElementsByClassName('pipetype');
    let pipemarr = document.getElementsByClassName('pipepm');
    let pipecostarr = document.getElementsByClassName('pipecost');
    let nectypearr = document.getElementsByClassName('nectype');
    let necsqrarr = document.getElementsByClassName('necsqr');
    let neccostarr = document.getElementsByClassName('neccost');
    let lenarray = document.getElementsByClassName('lenirow');
    let countarray = document.getElementsByClassName('countirow');
    let delarray = document.getElementsByClassName('deldiv');
    let delmisc = document.getElementsByClassName('delmisc');
    let delpipe = document.getElementsByClassName('delpipe');
    let delnecond = document.getElementsByClassName('delnecond');
    while (k < lenarray.length) {
        lenarray[k].readOnly = false;
        countarray[k].readOnly = false;
        delarray[k].innerHTML += "<button type='button' onclick='deletePos(this.parentNode.id)'>Х</button>";
        k++;
    }
    k = 0;
    while (k < miscprodarr.length) {
        miscprodarr[k].readOnly = false;
        misccountarr[k].readOnly = false;
        misccostarr[k].readOnly = false;
        delmisc[k].innerHTML += "<button type='button' onclick='deleteMisc(this.parentNode.id)'>Х</button>";
        k++;
    }
    k = 0;
    while (k < pipetypearr.length) {
        pipetypearr[k].readOnly = false;
        pipemarr[k].readOnly = false;
        pipecostarr[k].readOnly = false;
        delpipe[k].innerHTML += "<button type='button' onclick='deletePipe(this.parentNode.id)'>Х</button>"
        k++;
    }
    k = 0;
    while (k < nectypearr.length) {
        nectypearr[k].readOnly = false;
        necsqrarr[k].readOnly = false;
        neccostarr[k].readOnly = false;
        delnecond[k].innerHTML += "<button type='button' onclick='deleteNecond(this.parentNode.id)'>Х</button>"
        k++;
    }
    document.querySelector('.foot').innerHTML = save_btn + cancelChange_btn + addSheet + addMisc + addPipe + addNecond;
}


function changeSqr(fid) {
    var x = String(fid.match(/\d+/g));
    (posToChange.indexOf(String(x)) === -1 && posToCreate.indexOf(String(x)) === -1) ? posToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('postochange').value = posToChange;
    let wave = document.querySelector('.wavetype').innerHTML;
    let coef = getWaveCoef(wave);
    let sqrfield = document.getElementById('sqr' + x);
    let fsqrfield = document.getElementById('fsqr' + x);
    let countfield = document.getElementById('counti' + x);
    let lenfield = document.getElementById('leni' + x);
    let costfield = document.getElementById('cost' + x);
    let icostfield = document.getElementById('icost' + x);
    let pmfield = document.getElementById('pm' + x);
    pm = lenfield.value * countfield.value;
    icost = icostfield.innerHTML;

    sqr = coef * lenfield.value;
    fsqr = sqr * countfield.value;
    cost = fsqr * icost;
    pmfield.value = Number(pm.toFixed(3));
    sqrfield.value = Number(sqr.toFixed(3));
    fsqrfield.value = Number(fsqr.toFixed(3));
    costfield.value = Number(cost.toFixed(3));
    countFinal();

}

function countFinal() {
    let countarray = document.getElementsByClassName('countirow');
    let sqrarray = document.getElementsByClassName('fsqrrow');
    let costarray = document.getElementsByClassName('costrow');
    let pmarray = document.getElementsByClassName('pmrow');
    let fcountfield = document.getElementById('fcount');
    let fsqrfield = document.getElementById('fsqr');
    let fcostfield = document.getElementById('fcost');
    let fpmfield = document.getElementById('fpm');
    fcount = 0;
    fsqr = 0;
    fpm = 0;
    fcost = 0;
    var i = 0;
    while (i < countarray.length) {
        fcost += Number(costarray[i].value);
        fsqr += Number(sqrarray[i].value);
        fpm += Number(pmarray[i].value);
        fcount += Number(countarray[i].value);

        i++;
    }
    fcountfield.value = Number(fcount);
    fsqrfield.value = Number(fsqr.toFixed(3));
    fcostfield.value = Number(fcost.toFixed(3));
    fpmfield.value = Number(fpm.toFixed(3));
    calculateFinalCost();
}

function getWaveCoef(a) {
    let coef = new Number();
    if (a == "С-8") coef = 1.2;
    if (a == "С-20") coef = 1.14;
    if (a == "С-21") coef = 1.051;
    if (a == "НС-35") coef = 1.09;
    if (a == "МП-20") coef = 1.15;
    if (a == "Н-60") coef = 0.902;
    if (a == "С-8 АССИМ") coef = 1.18;
    if (a == "ГЛАДКИЙ ЛИСТ") coef = 1.25;
    return coef;
}

function getSqr(wave) {
    let ret = new String();
    if (wave == "С-8") ret = "(1.2/1.15)";
    if (wave == "С-20") ret = "(1.14/1.08)";
    if (wave == "С-21") ret = "(1.051/1)";
    if (wave == "С-8 АССИМ") ret = "(1.18/1.13)";
    if (wave == "НС-35") ret = "(1.09/1)";
    if (wave == "МП-20") ret = "(1.15/1.10)";
    if (wave == "Н-60") ret = "(0.902/0.845)";
    return ret;
}


function calculateMisc(fid) {
    var x = String(fid.match(/\d+/g));
    (miscToChange.indexOf(String(x)) === -1 && miscToAdd.indexOf(String(x)) === -1) ? miscToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('misctochange').value = miscToChange;
    var miscfcost = document.getElementById('miscfcost' + x);
    var misccost = document.getElementById('misccost' + x);
    var misccount = document.getElementById('misccount' + x);
    miscfcost.value = Number(misccost.value) * Number(misccount.value);
    calculateFinalCost();
}

function calculatePipe(fid) { //RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    let x = String(fid.match(/\d+/g));
    (pipeToChange.indexOf(String(x)) === -1 && pipeToAdd.indexOf(String(x)) === -1) ? pipeToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('pipetochange').value = pipeToChange;
    let pipefcost = document.getElementById('pipefcost' + x);
    let pipecost = document.getElementById('pipecost' + x);
    let pipepm = document.getElementById('pipepm' + x);
    pipefcost.value = Number(pipepm.value) * Number(pipecost.value);
    calculateFinalCost();
}

function calculateNecond(fid) {
    let x = String(fid.match(/\d+/g));
    (necondToChange.indexOf(String(x)) === -1 && necondToAdd.indexOf(String(x)) === -1) ? necondToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('nectochange').value = necondToChange;
    let necfcost = document.getElementById('necfcost' + x);
    let neccost = document.getElementById('neccost' + x);
    let necsqr = document.getElementById('necsqr' + x);
    necfcost.value = Number(necsqr.value) * Number(neccost.value);
    calculateFinalCost();
}

function calculateFinalCost() {
    let poscostarr = document.getElementsByClassName('costrow');
    let misccostarr = document.getElementsByClassName('miscfcost');
    let pipecostarr = document.getElementsByClassName('pipefcost');
    let neccostarr = document.getElementsByClassName('necfcost');
    let fcost = document.getElementById('fullcost');
    let fcost1 = document.getElementById('finalcost1');
    var res = 0;
    var k = 0;
    while (k < poscostarr.length) {
        res += Number(poscostarr[k].value);
        k++;
    }
    k = 0;
    while (k < misccostarr.length) {
        res += Number(misccostarr[k].value);
        k++;
    }
    k = 0;
    while (k < pipecostarr.length) {
        res += Number(pipecostarr[k].value);
        k++;
    }
    k = 0;
    while (k < neccostarr.length) {
        res += Number(neccostarr[k].value);
        k++;
    }
    fcost.innerHTML = Number(res.toFixed(3));
    fcost1.value = Number(res.toFixed(3));
}

function deletePos(fid) {
    var x = String(fid.match(/\d+/g));
    let mrow = document.getElementById('mrow' + x);
    mrow.remove();
    posToDelete.push(String(x));
    if (posToChange.indexOf(String(x)) !== -1) {
        let place = posToChange.indexOf(String(x));
        posToChange.splice(place, 1);
    }
    document.getElementById('postochange').value = posToChange;
    document.getElementById('postodelete').value = posToDelete;
    countFinal();
}

function deleteMisc(fid) {
    var x = String(fid.match(/\d+/g));
    let mirow = document.getElementById('mirow' + x);
    mirow.remove();
    miscToDelete.push(String(x));
    if (miscToChange.indexOf(String(x)) !== -1) {
        let place = miscToChange.indexOf(String(x));
        miscToChange.splice(place, 1);
    }
    document.getElementById('misctodelete').value = miscToDelete;
    document.getElementById('misctochange').value = miscToChange;
    calculateFinalCost();

}

function deletePipe(fid) {
    var x = String(fid.match(/\d+/g));
    let pirow = document.getElementById('pirow' + x);
    pirow.remove();
    pipeToDelete.push(String(x));
    if (pipeToChange.indexOf(String(x)) !== -1) {
        let place = pipeToChange.indexOf(String(x));
        pipeToChange.splice(place, 1);
    }
    document.getElementById('pipetodelete').value = pipeToDelete;
    document.getElementById('pipetochange').value = pipeToChange;
    calculateFinalCost();
}

function deleteNecond(fid) {
    var x = String(fid.match(/\d+/g));
    let necrow = document.getElementById('necrow' + x);
    necrow.remove();
    necondToDelete.push(String(x));
    if (necondToChange.indexOf(String(x)) !== -1) {
        let place = necondToChange.indexOf(String(x));
        necondToChange.splice(place, 1);
    }
    document.getElementById('nectodelete').value = necondToDelete;
    document.getElementById('nectochange').value = necondToChange;
    calculateFinalCost();
}

function updatePage() {
    document.location.reload(true);
}

function redirectTo() {
    document.location.href = '/deleteorder/' + document.getElementById(order.id);
}

function onError(errorStatus, errorMessage) {
    let xml = new XMLHttpRequest();
    xml.open('POST', '/clienterror', true);
    let send = { status: errorStatus, message: errorMessage };
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(send));
}

function miscChange(fid) {
    let x = String(fid.match(/\d+/g));
    (miscToChange.indexOf(String(x)) === -1) ? miscToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('misctochange').value = miscToChange;
}

function pipeChange(fid) {
    let x = String(fid.match(/\d+/g));
    (pipeToChange.indexOf(String(x)) === -1) ? pipeToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('pipetochange').value = pipeToChange;
}

function necondChange(fid) {
    let x = String(fid.match(/\d+/g));
    (necondToChange.indexOf(String(x)) === -1) ? necondToChange.push(String(x)): console.log("This item already exists");
    document.getElementById('nectochange').value = necondToChange;
}

///////////////////////////
///// PRINT FUNCTIONS /////
///////////////////////////

function clickPrintBirka() {
    changePrintSize('portrait');

    let tag = document.querySelector('.tag-content');
    let container = document.querySelector('.container2');
    let ordern = document.getElementById("ordern").innerHTML;
    let client = document.querySelector('.client').innerHTML;
    let fsize = "80px;";
    if (client.length > 16) {
        fsize = "60px;"
    }
    let fcount = document.getElementById("fcount").innerHTML;
    let fsqr = document.getElementById("fsqr").value;
    let wavetype = document.querySelector('.wavetype').innerHTML;
    let color = document.querySelector('.color').innerHTML;
    let afterwave = document.querySelector('.afterwave').innerHTML;
    let manager = document.getElementById('manager').innerHTML;

    if (printBirka == 0) {
        container.hidden = true;
        tag.innerHTML = "<p id='tagorder'>" + ordern + "</p><p id='tagclient' style='font-size:" + fsize + "'>" + client + "</p><p id='tagwave'>" + wavetype + afterwave + "</p><p id='tagsqr'>" + fsqr + " м2</p><p id='tagthcolor'> " + color + "</p><p id='tagmanager'>" + manager + "</p>";
        printBirka++;
    } else {
        container.hidden = false;
        tag.innerHTML = " ";
        printBirka--;
    }
    window.print();
}

function clickPrint(pr) {
    console.log(fthick);
    changePrintSize('landscape');
    if (pr == 'pr') {
        if (fthick == '0.25') document.querySelector('.thick').value = '0 дп';
        if (fthick == '0.27') document.querySelector('.thick').value = '0 дс';
        if (fthick == '0.3') document.querySelector('.thick').value = '0 т';
        if (fthick == '0.33') document.querySelector('.thick').value = '0 тт';
        if (fthick == '0.35') document.querySelector('.thick').value = '0 тп';
        if (fthick == '0.38') document.querySelector('.thick').value = '0 тв';
        if (fthick == '0.4') document.querySelector('.thick').value = '0 ч';
        if (fthick == '0.43') document.querySelector('.thick').value = '0 ст';
        if (fthick == '0.45') document.querySelector('.thick').value = '0 сп';
        if (fthick == '0.5') document.querySelector('.thick').value = '0 п';
        if (fthick == ' 0.55') document.querySelector('.thick').value = '0 пп';
        if (fthick == '0.6') document.querySelector('.thick').value = '0 ш';
        if (fthick == '0.65') document.querySelector('.thick').value = '0 шп';
        if (fthick == '0.7') document.querySelector('.thick').value = '0 сем';
        if (fthick == '0.75') document.querySelector('.thick').value = '0 семп';
        if (fthick == '0.8') document.querySelector('.thick').value = '0 в';
        if (fthick == '0.85') document.querySelector('.thick').value = '0 вп';
    }
    if (pr == 'nepr') {
        if (thick1 == '0.25') document.querySelector('.thick').value = 'Толщина: 0.25';
        if (thick1 == '0.27') document.querySelector('.thick').value = 'Толщина: 0.27';
        if (thick1 == '0.3') document.querySelector('.thick').value = 'Толщина: 0.3';
        if (thick1 == '0.33') document.querySelector('.thick').value = 'Толщина: 0.33';
        if (thick1 == '0.35') document.querySelector('.thick').value = 'Толщина: 0.35';
        if (thick1 == '0.38') document.querySelector('.thick').value = 'Толщина: 0.38';
        if (thick1 == '0.4') document.querySelector('.thick').value = 'Толщина: 0.4';
        if (thick1 == '0.43') document.querySelector('.thick').value = 'Толщина: 0.43';
        if (thick1 == '0.45') document.querySelector('.thick').value = 'Толщина: 0.45';
        if (thick1 == '0.5') document.querySelector('.thick').value = 'Толщина: 0.5';
        if (thick1 == ' 0.55') document.querySelector('.thick').value = 'Толщина: 0.55';
        if (thick1 == '0.6') document.querySelector('.thick').value = 'Толщина: 0.6';
        if (thick1 == '0.65') document.querySelector('.thick').value = 'Толщина: 0.65';
        if (thick1 == '0.7') document.querySelector('.thick').value = 'Толщина: 0.7';
        if (thick1 == '0.75') document.querySelector('.thick').value = 'Толщина: 0.75';
        if (thick1 == '0.8') document.querySelector('.thick').value = 'Толщина: 0.8';
        if (fthick == '0.85') document.querySelector('.thick').value = 'Толщина: 0.85';
    }
    let btnpr = document.querySelector('.pr');

    let container = document.querySelector('.container2');
    let body = document.body;
    let maincontent = document.querySelector('.maincontent');
    let menu = document.querySelector('.sidemenu');
    let delbtn = document.querySelector('.foot');
    let printable = document.querySelector('.printable');
    let top = document.querySelector('.top1');

    let backbtn = document.getElementById('backButton');
    if (printflag == 0) {
        backbtn.hidden = true;
        btnpr.hidden = true;
        document.querySelector('.birka').hidden = true;
        delbtn.hidden = true;
        menu.hidden = true;
        top.innerHTML += "<span style='font-size:20px; margin-left:auto;margin-right:auto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Режим работы: ПН-ПТ 9:00 - 18:00   Перерыв: 13:00 - 14:00. +7928 957 09 68 - Светлана</span>";
        maincontent.style.marginLeft = "0px";
        printable.style.display = "block";
        container.style.marginLeft = "5px";
        container.style.marginTop = "5px";
        container.style.width = "100%";
        printable.style.height = "100px";
        body.style.backgroundColor = "white";
        window.print();
        printflag++;

    } else {
        backbtn.hidden = false;
        btnpr.hidden = false;
        top.innerHTML = "<span style='font-size:20px'>ООО 'Юг-Профиль'</span > ";
        document.querySelector('.birka').hidden = false;
        maincontent.style.marginLeft = "250px";
        printable.style.height = "0";
        printable.style.display = "none";
        container.style.marginLeft = "0px";
        container.style.marginTop = "0px";
        delbtn.hidden = false;
        menu.hidden = false;
        container.style.width = "100%";
        printflag--;

    }

}

function changePrintSize(orientation) {
    var css = '@page { size: ' + orientation + '; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet) {
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);

}

//////////////////////
///// PROTOTYPES /////
//////////////////////

String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}