var req = new XMLHttpRequest();
var url = '/getincomedata';
let income;
req.open('POST', url, true);
req.addEventListener('load', onLoad);
req.addEventListener('error', onError);
req.send();

function onLoad() {
    let i = 0;
    var response = this.responseText;
    income = JSON.parse(response);
    console.log(income);
    let color = "#000";

    while (i < income.id.length) {
        if (income.status[i] === 2) {

            if (income.color[i] == "3005 Красный") color = "#59191f";
            if (income.color[i] == "5005 Синий") color = "#005387";
            if (income.color[i] == "8017 Коричневый") color = "#442f29";
            if (income.color[i] == "6005 Зеленый") color = "#114232";
            if (income.color[i] == "1014 Слоновая кость") color = "#ddc49a";
            if (income.color[i] == "5021 Бирюзовый") color = "#007577";
            if (income.color[i] == "9003 Белый") color = "#ecece7";
            if (income.color[i] == "7004 Серый") color = "#9b9b9b";
            if (income.color[i] == "6002 Светло-зел") color = "#325928";
            if (income.color[i] == "Дерево") color = "rgba(0,0,0,0)";
            if (income.color[i] == "Камень") color = "rgba(0,0,0,0)";
            if (income.color[i] == "Цинк") color = "#bac4c8";

            var row = document.createElement("tr");
            row.setAttribute("onClick", "loadPage(" + income.id[i] + ")");
            let d1 = new Date(income.acceptdate[i]).toLocaleString();
            var ctime = d1.replace('T', ' ');
            ctime = ctime.slice(0, 20);
            row.id = 'row' + i;
            row.innerHTML = "<td>" + income.id[i] + "</td><td>" + income.supply[i] + "</td><td>" + income.thickness[i] + "</td>\
            <td style='display:flex'><span style='font-weight:bold;font-size:35px;color:" + color + "'>&#9607&nbsp</span>" + income.color[i] + "</td>\
            <td>" + income.weight[i] + "</td><td>" + income.pm[i] + "</td><td>" + income.pmfull[i] + "</td><td>" + income.wagon[i] + "</td>\
            <td>" + income.prinyal[i] + "</td><td>" + ctime + "<form id='page_submit" + income.id[i] + "' method='GET' action='/roll_page/" + income.id[i] + "'></form></td>";
            document.getElementById("node1").appendChild(row);
        }
        i++;

    }
}

function loadPage(id) {
    form = document.getElementById('page_submit' + id);
    console.log(id);
    form.submit();
}

function onError(errorStatus, errorMessage) {
    let xml = new XMLHttpRequest();
    xml.open('POST', '/clienterror', true);
    let send = { status: errorStatus, message: errorMessage };
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(send));
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}