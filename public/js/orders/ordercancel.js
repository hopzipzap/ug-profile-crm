curl = window.location.href;

var final = curl.match(/\/([^\/]+)\/?$/)[1];
if (final > 0)
    window.location.replace('/order_page/' + final);
var req = new XMLHttpRequest();
var url = '/getorderdata';
req.open('POST', url, true);
req.addEventListener('load', onLoad);
req.addEventListener('error', onError);
req.send();

function onLoad() {
    var i = 0;
    var response = this.responseText;
    var order = JSON.parse(response);
    var role = document.getElementById('session-role').innerHTML;
    var name = document.getElementById('session-name').innerHTML;
    while (i < order.id.length) {
        if (((role == 'Администратор' || role == 'Менеджер производства') || name == order.manager[i]) && order.status[i] == 3) {
            var row = document.createElement("tr");
            row.id = 'row' + order.id[i];
            let d1 = new Date(order.createdate[i]).toLocaleString();
            row.setAttribute("onClick", "loadPage(" + order.id[i] + ")");
            let departdate = new Date(order.readydate[i]).toLocaleString().slice(0, 10);
            var ctime = d1.replace('T', ' ');
            var fstat;
            let color = "#000";
            order.finalsqr[i] = Number(order.finalsqr[i]);
            if (order.color[i] == "3005 Красный") color = "#59191f";
            if (order.color[i] == "5005 Синий") color = "#005387";
            if (order.color[i] == "8017 Коричневый") {
                color = "#442f29";
                order.color[i] = "8017 Коричн.";
            }

            if (order.color[i] == "6005 Зеленый") color = "#114232";
            if (order.color[i] == "1014 Слоновая кость") color = "#ddc49a";
            if (order.color[i] == "5021 Бирюзовый") color = "#007577";
            if (order.color[i] == "9003 Белый") color = "#ecece7";
            if (order.color[i] == "7004 Серый") color = "#9b9b9b";
            if (order.color[i] == "6002 Светло-зел") {
                order.color[i] = "6002 Светло-зел.";
                color = "#325928";
            }
            if (order.color[i] == "Дерево") color = "rgba(0,0,0,0)";
            if (order.color[i] == "Камень") color = "rgba(0,0,0,0)";
            if (order.color[i] == "Цинк") color = "#bac4c8";
            if (order.status[i] == 0) fstat = '&#9203 Ожидает';
            if (order.status[i] == 1) fstat = '&#9889 В производстве';
            if (order.status[i] == 2) fstat = '&#9989 Готов';
            if (order.status[i] == 3) fstat = '&#10060 Отменен';
            if (order.status[i] == 4) fstat = '&#128666 Отгружен';
            if (order.wavetype[i] == 'НЕКОНДИЦИЯ') {
                order.wavetype[i] == 'НЕКОНД.'
                order.color[i] = '---//---';
                order.fthick[i] = '---//---';
                order.finalsqr[i] = '---//---';
            }
            if (order.wavetype[i] == 'ДРУГОЕ') {
                order.wavetype[i] == 'ДРУГОЕ'
                order.color[i] = '---//---';
                order.fthick[i] = '---//---';
                order.finalsqr[i] = '---//---';
            }

            if (order.client[i].length > 16)
                order.client[i] = order.client[i].slice(0, 16);
            var ctime = ctime.slice(0, 20);
            row.innerHTML = "<td>" + order.id[i] + "</td><td>" + order.wavetype[i] + "</td><td style='display:flex'><span style='font-weight:bold;font-size:35px;color:" + color + "'>&#9607&nbsp</span>" + order.color[i] + "</td><td>" + order.fthick[i] + "</td><td>" + order.finalsqr[i] + "</td><td>" + ctime + "</td><td>" + departdate + "</td><td>" + order.client[i] + "</td><td>" + order.manager[i] + "</td><td>" + fstat + "<form id='page_submit" + order.id[i] + "' method='GET' action='/order_page/" + order.id[i] + "'></form></td>"
            "</td>";
            document.getElementById("node").appendChild(row);
        }
        i++;
    }
}

function loadPage(id) {
    form = document.getElementById('page_submit' + id);
    console.log(id);
    form.submit();
}

function onError(errorStatus, errorMessage) {
    let xml = new XMLHttpRequest();
    xml.open('POST', '/clienterror', true);
    let send = { status: errorStatus, message: errorMessage };
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(send));
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}