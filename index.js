////////////////
//REQUIREMENTS//
////////////////

var _ = require('lodash');
var _util = require('./utility/utility');
var _err = require('./utility/errors');
const express = require('express');
const mysql = require('mysql2');
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const fs = require('fs');
const http = require('http');
const https = require('https');
const createError = require('http-errors');
const multer = require('multer');
const path = require('path');

//////////////
//SQLOBJECTS//
//////////////

let orderid;
let rullonid;
let misc = {
    id: [],
    uniqid: [],
    product: [],
    count: [],
    cost: [],
    fcost: []
}

let pipe = {
    id: [],
    uniqid: [],
    type: [],
    pm: [],
    cost: [],
    fcost: [],
}

let necond = {
    id: [],
    uniqid: [],
    type: [],
    sqr: [],
    cost: [],
    fcost: [],
}

let positions = {
    id: [],
    uniqid: [],
    length: [],
    count: [],
    sqr: [],
    cost: [],
    costpm: [],
}

let income = {
    id: [],
    supply: [],
    thickness: [],
    color: [],
    weight: [],
    pm: [],
    pmfull: [],
    wagon: [],
    prim: [],
    prinyal: [],
    acceptdate: [],
    status: [], // 0 - закрыт 1 - открыт 2 - списан
    passimgname: [],
    birkaimgname: [],
    discarddate: []
}

let order = {
    id: [],
    wavetype: [],
    color: [],
    thick: [],
    fthick: [],
    createdate: [],
    readydate: [],
    manager: [],
    client: [],
    status: [],
    fthick: [],
    uniqid: [],
    finalcost: [],
    finalcount: [],
    finalsqr: [],
    finallen: [],
    misc: [],
    operator: [],
    roll: [],
    payment: [],
    phonenumber: [],
    cost: [],
    departdate: [],
    productiondate: [],
    finishdate: []
};

///////////////////////////
//Create mysql connection//
///////////////////////////

const connection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ugprofile'
});

/////////////////
//EXPRESSPARAMS//
/////////////////

let app = express();

// const privateKey = fs.sareadFileSync('/etc/letsencrypt/live/mng.ugprofile.ru/privkey.pem', 'utf8');
// const certificate = fs.readFileSync('/etc/letsencrypt/live/mng.ugprofile.ru/cert.pem', 'utf8');
// const ca = fs.readFileSync('/etc/letsencrypt/live/mng.ugprofile.ru/chain.pem', 'utf8');

// const credentials = {
//     key: privateKey,
//     cert: certificate,
//     ca: ca
// }

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.set("views", __dirname + "/views");
app.set("views", __dirname + "/views");
app.use('/js', express.static(__dirname + '/public/js/orders'));
app.use('/js', express.static(__dirname + '/public/js/rolls'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/js', express.static(__dirname + '/node_modules/@fancyapps/fancybox/dist')); // fancybox JS jQuery
app.use('/css', express.static(__dirname + '/node_modules/@fancyapps/fancybox/dist')); // fancybox CSS jQuery
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap
app.use('/js', express.static(__dirname + '/node_modules/bootstrap-select/dist/js')); // redirect bootstrap JS
app.use('/css', express.static(__dirname + '/node_modules/bootstrap-select/dist/css')); // redirect CSS bootstrap
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist/umd')); // redirect CSS bootstrap
app.use(session({
    cookie: {
        maxAge: 60 * 60 * 1000 * 24
    },
    secret: '&73sthezgcomost=_66vesecretn*4aewmkkeyyxz23of_%+72all5+t=pkeyszjv#b@f%we(r3',
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const storageConfig = multer.diskStorage({ //SETUP MULTER
    destination: (req, file, callback) => {
        callback(null, 'public/img/acceptuploads');
    },
    filename: (req, file, callback) => {
        let uniqid = (new Date()).getTime().toString(36) + Math.random().toString(36).slice(2);
        let extArray = file.mimetype.split("/");
        callback(null, uniqid + '.' + extArray[extArray.length - 1]);
    }
});

const fileFilter = (req, file, callback) => {

    if (file.mimetype === "image/png" ||
        file.mimetype === "image/jpg" ||
        file.mimetype === "image/jpeg") {
        callback(null, true);
    } else {
        callback(null, false);
    }
};

const upload = multer({
    storage: storageConfig,
    fileFilter: fileFilter
});

//////////////
//GET ROUTES//
//////////////

app.get('/', function(request, response) { //LOGIN PAGE 
    if (request.session.loggedin)
        response.redirect('/home');
    else
        response.render("index.ejs");
});

app.get('/home', _util.checkSession, function(request, response) { //HOME PAGE
    if (request.session.role == 0 || request.session.role == 1 || request.session.role == 4 || request.session.role == 10)
        response.redirect('/myorderlist');
    if (request.session.role == 2 || request.session.role == 3) {
        response.redirect('/rollaccept');
    }
});

app.get('/auth', _util.checkSession, function(request, response) { //AUTH 
    response.redirect('/home');
});

app.get('/createorder', _util.checkSession, function(request, response) { //ORDER CREATE PAGE
    response.render("orders/createorder.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});

app.get('/closedrolls', _util.checkSession, function(request, response) { //CLOSED INCOME LIST PAGE
    response.render("rolls/closedrolls.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});
app.get('/openrolls', _util.checkSession, function(request, response) { //OPEN INCOME LIST PAGE
    response.render("rolls/openrolls.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});
app.get('/discardedrolls', _util.checkSession, function(request, response) { //DISCARDED INCOME LIST PAGE
    response.render("rolls/discardedrolls.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});

app.get('/roll_page/:id', _util.checkSession, function(request, response) { //ROLL_PAGE/id
    rullonid = request.params.id;

    response.render("rolls/rollpage.ejs", { username: request.session.username, rullonid: rullonid, role: _util.getRole(request.session.role) });
});

app.get('/orderlist/:id', _util.checkSession, function(request, response, next) { //ORDERLIST PAGE
    order = _util.toNull(order);
    response.render("orders/orderlist.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });


});

app.get('/createuser', _util.checkSession, function(request, response) { //ADD USER PAGE
    if (request.session.role == 0)
        response.render("createuser.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
    else
        response.redirect('/home');
});
app.get('/rollaccept', _util.checkSession, function(request, response) { //ACCEPT INCOME PAGE
    role = _util.getRole(request.session.role);
    if (role == 'Администратор' || role == 'Кладовщик' || role == 'Менеджер по отгрузкам')
        response.render('rolls/rollaccept.ejs', { username: request.session.username, role: role })
    else
        response.redirect('/home');
});

app.get('/leftovers', _util.checkSession, function(request, response) {
    response.render('rolls/leftovers.ejs', { username: request.session.username, role: _util.getRole(request.session.role) });
});

app.get('/orderlist', _util.checkSession, function(request, response) {
    response.redirect('/orderlist/0');
});

app.get('/exitsession', _util.checkSession, function(request, response) { //EXIT SESSION GET REQUEST
    request.session.destroy();
    response.redirect('/');
});

app.get('/order_page/:id', _util.checkSession, function(request, response) { //ORDER_PAGE/id
    orderid = request.params.id;

    response.render("orders/orderpage.ejs", { username: request.session.username, orderid: orderid, role: _util.getRole(request.session.role) });
});

app.get('/cancelorder/:id', _util.checkSession, function(request, response) { //CANCEL ORDER REQUEST
    let pid = request.params.id;
    connection.query(`UPDATE orders SET status = 3 where id = ?`, [pid], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    response.redirect('/order_page/' + request.params.id);
});

app.get('/acceptproduction/:id', _util.checkSession, function(request, response) { //CANCEL ORDER REQUEST
    let pid = request.params.id;
    connection.query(`UPDATE orders SET status = 5 where id = ?`, [pid], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    response.redirect('/order_page/' + request.params.id);
});

app.get('/changestatus1/:id', _util.checkSession, function(request, response) { //CHANGE STATUS TO IN PRODUCTION REQUEST
    var date = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    connection.query(`UPDATE orders SET status = 1,productiondate = ? WHERE id = ?`, [date, request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    })
    response.redirect('/order_page/' + request.params.id);
});

app.get('/changestatus0/:id', _util.checkSession, function(request, response) { //CHANGE STATUS TO WAIT REQUEST
    connection.query(`UPDATE orders SET status = 0 WHERE id = ?`, [request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    })
    response.redirect('/order_page/' + request.params.id);
});

app.get('/cancelstatus1/:id', _util.checkSession, function(request, response) { //RETURN FROM PRODUCTION
    connection.query(`UPDATE orders SET status = 0 WHERE id = ?`, [request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    })
    response.redirect('/order_page/' + request.params.id);
});

app.get('/order_page1/:d', _util.checkSession, function(request, response) {
    response.redirect('/order_page/' + request.params.id);
});

app.get('/discardroll/:id', _util.checkSession, function(request, response) {
    var date = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    connection.query(`UPDATE rullon SET status = 2,discarddate = ? WHERE id = ?`, [date, request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    response.redirect('/closedrolls');
});

app.get('/orderdepart', _util.checkSession, function(request, response) {
    response.render("orders/orderdepart.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});

app.get('/myorderlist', _util.checkSession, function(request, response) {
    response.render("orders/myorderlist.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});

app.get('/orderinprod', _util.checkSession, function(request, response) {
    response.render("orders/orderproduction.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});
app.get('/orderready', _util.checkSession, function(request, response) {
    response.render("orders/orderready.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});
app.get('/ordercancel', _util.checkSession, function(request, response) {
    response.render("orders/ordercancel.ejs", { username: request.session.username, role: _util.getRole(request.session.role) });
});




app.get('/changestatusdepart/:id', _util.checkSession, function(request, response) {
    let departdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    connection.query(`UPDATE orders SET status = 4, departdate = ? WHERE id = ?`, [departdate, request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    response.redirect('/orderlist/0');
});

/////////////////
//POST REQUESTS//
/////////////////

app.post('/ordercreate', _util.checkSession, function(request, response) { //create order post request
    let i = 0;
    let finallen = request.body['finallen'];
    let finalcount = request.body['finalcount'];
    let finalcost = request.body['finalcost'];
    let finalsqr = request.body['finalsqr'];
    let manager = request.session.username;
    let costpm = request.body['costpm'];
    let wave = request.body['wavetype'];
    let color = request.body['mcolor'];
    let thickness = request.body['thickness'];
    let payment = request.body['payment'];
    let fthickness = request.body['fthickness'];
    let client = request.body['client'];
    let phonenumber = request.body['number'];
    let currentdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    let rowcount = request.body['rowcount'];
    let rowids = request.body['rowids'].split(',');
    let rowmiscids = request.body['rowmiscids'].split(',');
    let rowmisccount = rowmiscids.length;
    let pipeids = request.body['rowpipeids'].split(',');
    let pipecount = pipeids.length;
    let necondids = request.body['rownecondids'].split(',');
    let necondcount = necondids.length;
    let misc = request.body['misc'];
    let date = new Date(request.body.date).addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    let uniqid = (new Date()).getTime().toString(36) + Math.random().toString(36).slice(2);
    if (request.body['input-pipe' + pipeids[0]] != '') {
        while (i < pipecount) {
            connection.query(`INSERT INTO pipe(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['input-pipe' + pipeids[i]], request.body['pipepm' + pipeids[i]], request.body['pipecpm' + pipeids[i]], request.body['pipefc' + pipeids[i]], uniqid], function(error) {
                if (error)
                    console.log('pipe error ' + error);
            });
            i++;
        }
    }
    i = 0;
    if (request.body['input-necond' + necondids[0]] != '') {
        while (i < necondcount) {
            connection.query(`INSERT INTO necond(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['input-necond' + necondids[i]], request.body['necondpm' + necondids[i]], request.body['necondcpm' + necondids[i]], request.body['necondfc' + necondids[i]], uniqid], function(error) {
                if (error)
                    console.log('necond error ' + error);
            });
            i++;
        }
    }
    i = 0;
    if (request.body['inputMisc' + rowmiscids[0]] != '') {
        while (i < rowmisccount) {
            connection.query(`INSERT INTO misc(product,count,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['inputMisc' + rowmiscids[i]], request.body['inputMcount' + rowmiscids[i]], request.body['costM' + rowmiscids[i]], request.body['fcostM' + rowmiscids[i]], uniqid], function(error) {
                if (error)
                    console.log('misc error' + error);
            });
            i++;
        }
    }
    i = 0;
    if (wave != 'ДРУГОЕ') {
        while (i < rowcount) {
            connection.query(`INSERT INTO positions(len,count,sqr,cost,uniqid,costpm) VALUES(?,?,?,?,?,?)`, [request.body['inputlen' + rowids[i]], request.body['inputcount' + rowids[i]], request.body['sqrr' + rowids[i]], request.body['cost' + rowids[i]], uniqid, costpm], function(error) {
                if (error)
                    console.log('pos error ' + error);
            });
            i++;
        }
    }
    connection.query(`INSERT INTO orders(wavetype,thick,fthick,color,createdate,readydate,uniqid,manager,finallen,finalcount,finalcost,finalsqr,client,misc,payment,phonenumber,cost) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [wave, thickness, fthickness, color, currentdate, date, uniqid, manager, finallen, finalcount, finalcost, finalsqr, client, misc, payment, phonenumber, costpm], function(error, results, fields) {
        if (error)
            _err.mysqlErrorHandler(error);

        response.redirect('/orderlist/' + results.insertId);
        response.end();
    });


});


app.post('/deleteorder/:id', _util.checkSession, function(request, response) { //DELETE ORDER REQUEST
    let pid = request.params.id;
    let uniqid = request.body['uniqid'];
    connection.query(`DELETE FROM positions WHERE uniqid = ?`, [uniqid], function(error, results) {
        if (error) _err.mysqlErrorHandler(err);
    });
    connection.query(`DELETE FROM misc WHERE uniqid = ?`, [uniqid], function(error, results) {
        if (error) _err.mysqlErrorHandler(err);
    });
    connection.query(`DELETE FROM pipe WHERE uniqid = ?`, [uniqid], function(error, results) {
        if (error) _err.mysqlErrorHandler(err);
    });
    connection.query(`DELETE FROM necond WHERE uniqid = ?`, [uniqid], function(error, results) {
        if (error) _err.mysqlErrorHandler(err);
    });
    connection.query(`DELETE FROM orders WHERE id = ?`, [pid], function(error, results) {
        order = _util.toNull(order);
    });
    response.redirect('/home');
});

app.post('/saveorder/:id', function(request, response) {
    let fsqr = request.body['fsqr'];
    let fcount = request.body['fcount'];
    let fpm = request.body['fpm'];
    let finalcost = request.body['finalcostmain'];
    let misctoadd = request.body['misctoadd'];
    let misctodelete = request.body['misctodelete'];
    let misctochange = request.body['misctochange'];
    let pipetoadd = request.body['pipetoadd'];
    let pipetochange = request.body['pipetochange'];
    let pipetodelete = request.body['pipetodelete'];
    let nectoadd = request.body['nectoadd'];
    let nectochange = request.body['nectochange'];
    let nectodelete = request.body['nectodelete'];
    let pipeaddarray;
    let pipedelarray;
    let pipechangearray;
    let necaddarray;
    let necdelarray;
    let necchangearray;
    let costpm = request.body['costpm'];
    if (misctodelete)
        miscdelarr = misctodelete.split(',');
    else miscdelarr = [];
    if (misctochange)
        miscchangearr = misctochange.split(',');
    else
        miscchangearr = [];
    if (misctoadd)
        miscaddarray = misctoadd.split(',');
    else
        miscaddarray = [];
    if (pipetoadd)
        pipeaddarray = pipetoadd.split(',');
    else
        pipeaddarray = [];
    if (pipetochange)
        pipechangearray = pipetochange.split(',');
    else
        pipechangearray = [];
    if (pipetodelete)
        pipedelarray = pipetodelete.split(',');
    else
        pipedelarray = [];
    if (nectoadd)
        necaddarray = nectoadd.split(',');
    else
        necaddarray = [];
    if (nectochange)
        necchangearray = nectochange.split(',');
    else
        necchangearray = [];
    if (nectodelete)
        necdelarray = nectodelete.split(',');
    else
        necdelarray = [];
    let postodelete = request.body['postodelete'];
    if (postodelete)
        delarray = postodelete.split(',');
    else delarray = [];
    let postochange = request.body['postochange'];
    if (postochange)
        posarray = postochange.split(',');
    else posarray = [];
    let postocreate = request.body['postocreate'];
    if (postocreate)
        createarray = postocreate.split(',');
    else createarray = [];
    let uniqid = request.body['uniqid'];
    var i = 0;
    var curid = request.params.id;

    connection.query(`UPDATE orders SET finalcount = ?, finallen = ? , finalsqr = ? , finalcost = ? WHERE id = ?`, [fcount, fpm, fsqr, finalcost, curid], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });

    while (i < miscdelarr.length) {
        connection.query(`DELETE FROM misc WHERE id = ?`, [miscdelarr[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < miscchangearr.length) {
        if (!request.body['miscprod' + miscchangearr[i]] ||
            !request.body['misccount' + miscchangearr[i]] ||
            !request.body['misccost' + miscchangearr[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`UPDATE misc SET product = ?, count = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['miscprod' + miscchangearr[i]], request.body['misccount' + miscchangearr[i]],
            request.body['misccost' + miscchangearr[i]], request.body['miscfcost' + miscchangearr[i]], miscchangearr[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < miscaddarray.length) {
        if (!request.body['miscprod' + miscaddarray[i]] ||
            !request.body['misccount' + miscaddarray[i]] ||
            !request.body['misccost' + miscchangearray[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`INSERT INTO misc(product,count,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['miscprod' + miscaddarray[i]],
            request.body['misccount' + miscaddarray[i]], request.body['misccost' + miscaddarray[i]],
            request.body['miscfcost' + miscaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);

        });
        i++;
    }
    i = 0;

    while (i < pipeaddarray.length) { //ADD PIPES TO SQL
        if (!request.body['pipetype' + pipeaddarray[i]] ||
            !request.body['pipepm' + pipeaddarray[i]] ||
            !request.body['pipecost' + pipeaddarray[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`INSERT INTO pipe(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['pipetype' + pipeaddarray[i]],
            request.body['pipepm' + pipeaddarray[i]], request.body['pipecost' + pipeaddarray[i]],
            request.body['pipefcost' + pipeaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < pipechangearray.length) { // CHANGE PIPE 
        if (!request.body['pipetype' + pipechangearray[i]] ||
            !request.body['pipepm' + pipechangearray[i]] ||
            !request.body['pipecost' + pipechangearray[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`UPDATE pipe SET type = ?, pm = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['pipetype' + pipechangearray[i]], request.body['pipepm' + pipechangearray[i]],
            request.body['pipecost' + pipechangearray[i]], request.body['pipefcost' + pipechangearray[i]], pipechangearray[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        connection.query(`DELETE FROM necond WHERE uniqid = ?`, [uniqid], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < necaddarray.length) { //ADD NECONDS TO SQL
        if (!request.body['nectype' + necaddarray[i]] ||
            !request.body['necsqr' + necaddarray[i]] ||
            !request.body['neccost' + necaddarray[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`INSERT INTO necond(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['nectype' + necaddarray[i]],
            request.body['necsqr' + necaddarray[i]], request.body['neccost' + necaddarray[i]],
            request.body['necfcost' + necaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
            object = _util.toNull(order);
        });
        i++;
    }
    i = 0;

    while (i < necchangearray.length) { // CHANGE NECONDS
        if (!request.body['nectype' + necchangearray[i]] ||
            !request.body['necsqr' + necchangearray[i]] ||
            !request.body['neccost' + necchangearray[i]])
            throw createError(500, "FIELD/s EMPTY");
        connection.query(`UPDATE necond SET type = ?, pm = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['nectype' + necchangearray[i]], request.body['necsqr' + necchangearray[i]],
            request.body['neccost' + necchangearray[i]], request.body['necfcost' + necchangearray[i]], necchangearray[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < necdelarray.length) { //DEL NECONDS
        connection.query(`DELETE FROM necond WHERE id = ?`, [necdelarray[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < delarray.length) {
        connection.query(`DELETE FROM positions WHERE id = ?`, [delarray[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
    }
    i = 0;

    while (i < miscdelarr.length) {
        connection.query(`DELETE FROM misc WHERE id = ?`, [miscdelarr[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < miscchangearr.length) {
        connection.query(`UPDATE misc SET product = ?, count = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['miscprod' + miscchangearr[i]], request.body['misccount' + miscchangearr[i]],
            request.body['misccost' + miscchangearr[i]], request.body['miscfcost' + miscchangearr[i]], miscchangearr[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < miscaddarray.length) {
        connection.query(`INSERT INTO misc(product,count,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['miscprod' + miscaddarray[i]],
            request.body['misccount' + miscaddarray[i]], request.body['misccost' + miscaddarray[i]],
            request.body['miscfcost' + miscaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < pipeaddarray.length) { //ADD PIPES TO SQL
        connection.query(`INSERT INTO pipe(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['pipetype' + pipeaddarray[i]],
            request.body['pipepm' + pipeaddarray[i]], request.body['pipecost' + pipeaddarray[i]],
            request.body['pipefcost' + pipeaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < pipechangearray.length) { // CHANGE PIPE 
        connection.query(`UPDATE pipe SET type = ?, pm = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['pipetype' + pipechangearray[i]], request.body['pipepm' + pipechangearray[i]],
            request.body['pipecost' + pipechangearray[i]], request.body['pipefcost' + pipechangearray[i]], pipechangearray[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < pipedelarray.length) { //DEL PIPE
        connection.query(`DELETE FROM pipe WHERE id = ?`, [pipedelarray[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < necaddarray.length) { //ADD NECONDS TO SQL
        connection.query(`INSERT INTO necond(type,pm,cost,fcost,uniqid) VALUES(?,?,?,?,?)`, [request.body['nectype' + necaddarray[i]],
            request.body['necsqr' + necaddarray[i]], request.body['neccost' + necaddarray[i]],
            request.body['necfcost' + necaddarray[i]], uniqid
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < necchangearray.length) { // CHANGE NECONDS
        connection.query(`UPDATE necond SET type = ?, pm = ? , cost = ? , fcost = ? WHERE id = ?`, [request.body['nectype' + necchangearray[i]], request.body['necsqr' + necchangearray[i]],
            request.body['neccost' + necchangearray[i]], request.body['necfcost' + necchangearray[i]], necchangearray[i]
        ], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < necdelarray.length) { //DEL NECONDS
        connection.query(`DELETE FROM necond WHERE id = ?`, [necdelarray[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < delarray.length) {
        connection.query(`DELETE FROM positions WHERE id = ?`, [delarray[i]], function(error, results) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    i = 0;

    while (i < posarray.length) {
        len = request.body['leni' + posarray[i]];
        count = request.body['counti' + posarray[i]];
        sqr = request.body['fsqr' + posarray[i]];
        cost = request.body['cost' + posarray[i]];
        if (len != '')
            connection.query(`UPDATE positions SET len = ?,count = ?, sqr = ?, cost = ? WHERE id = ?`, [len, count, sqr, cost, posarray[i]], function(error, results) {
                if (error) _err.mysqlErrorHandler(error);
            });
        i++;
    }
    i = 0;

    while (i < createarray.length) {
        len = request.body['leni' + createarray[i]];
        count = request.body['counti' + createarray[i]];
        sqr = request.body['fsqr' + createarray[i]];
        cost = request.body['cost' + createarray[i]];
        if (len != '')
            connection.query(`INSERT INTO positions(len,count,sqr,cost,uniqid,costpm) VALUES (?,?,?,?,?,?)`, [len, count, sqr, cost, uniqid, costpm], function(error, results) {
                if (error) _err.mysqlErrorHandler(error);
            });
        i++;
    }

    response.redirect('/order_page/' + request.params.id);

});

app.post('/auth', function(request, response) { //AUTH POST REQUEST

    let username = request.body.username;

    let password = request.body.password;

    if (username && password) {

        connection.query('SELECT * FROM users WHERE username = ?', [username], function(error, results, fields) {

            if (results.length > 0) {

                let cryptpass = bcrypt.hashSync(password, results[0].uniqid);

                if (cryptpass == results[0].password) {
                    request.session.loggedin = true;
                    request.session.username = results[0].lastname;
                    request.session.role = results[0].role;
                    response.redirect('/home');
                } else response.send('Неверный пароль');
            } else {
                response.send('неверный логин');
            }
            response.end();
        });
    } else {
        response.send('Пожалуйста введите логин и пароль!');
        response.end();
    }

});

app.post('/incacc', upload.array('filedata', 2), _util.checkSession, function(request, response) { //ACCEPT INCOME POST REQUEST
    let supply = request.body['supply'];
    let thickness = request.body['thickness'];
    let color = request.body['color'];
    let weight = request.body['weight'];
    let pm = request.body['pm'];
    let wagon = request.body['wagon'];
    let prim = request.body['prim'];
    let currentdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    let prinyal = request.session.username;
    let status = 0;
    let fileData = request.files;
    let currentRullonId;
    // console.log(fileData);
    if (!fileData)
        throw createError(500, 'Uploading file error');
    returnIncomeArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        currentRullonId = income.id[income.id.length - 1];
        const date = currentdate.split(':').join('-');
        const birkaType = request.files[0].mimetype.split("/");
        const passType = request.files[1].mimetype.split("/");
        const birkaName = ++currentRullonId + '_birka_' + date + '.' + birkaType[birkaType.length - 1];
        const passName = currentRullonId + '_pass_' + date + '.' + passType[passType.length - 1];
        const birkaPath = path.join(__dirname, 'public', 'img', 'acceptuploads', birkaName);
        const passPath = path.join(__dirname, 'public', 'img', 'acceptuploads', passName);
        fs.rename(request.files[0].path, birkaPath, function(err) {
            if (err)
                throw createError(err);
        });
        fs.rename(request.files[1].path, passPath, function(err) {
            if (err)
                throw createError(err);
        });
        connection.query(`INSERT INTO rullon(supply,thickness,color,weight,pm,wagon,prim,prinyal,acceptdate,pmfull,status,birkaimgname,passimgname) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`, [supply, thickness, color, weight, pm, wagon, prim, prinyal, currentdate, pm, status, birkaName, passName],
            function(error, results) {
                if (error) _err.mysqlErrorHandler(error);
                response.redirect('/closedrolls');
            });
    })).catch(error => createError(500, 'Internal Server Error'));

});


app.post('/getorder', _util.checkSession, function(req, res) { //GET ORDER DATA POSTREQUEST
    returnOrderArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        let id = 0;
        while (Number(order.id[id]) != Number(orderid)) id++;
        let ord = {
            id: order.id[id],
            wavetype: order.wavetype[id],
            color: order.color[id],
            thick: order.thick[id],
            fthick: order.fthick[id],
            createdate: order.createdate[id],
            readydate: order.readydate[id],
            manager: order.manager[id],
            client: order.client[id],
            status: order.status[id],
            fthick: order.fthick[id],
            uniqid: order.uniqid[id],
            finalcost: order.finalcost[id],
            finalcount: order.finalcount[id],
            finalsqr: order.finalsqr[id],
            finallen: order.finallen[id],
            misc: order.misc[id],
            operator: order.operator[id],
            roll: order.roll[id],
            payment: order.payment[id],
            phonenumber: order.phonenumber[id],
            cost: order.cost[id],
            departdate: order.departdate[id],
            productiondate: order.productiondate[id],
            finishdate: order.finishdate[id],
        };
        var data = JSON.stringify(ord);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/getmiscdata', function(req, res) { // GET MISC DATA POST REQUEST
    returnMiscArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(misc);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/getposdata', function(req, res) { //GET POSITIONS DATA POST REQUEST
    returnPositionsArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(positions);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/getpipedata', function(req, res) { //GET POSITIONS DATA POST REQUEST
    returnPipeArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(pipe);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/getneconddata', function(req, res) { //GET POSITIONS DATA POST REQUEST
    returnNecondArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(necond);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/getrolldata', _util.checkSession, function(req, res) {
    returnIncomeArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        let id = 0;
        while (Number(income.id[id]) != Number(rullonid)) id++;
        let roll = {
            id: income.id[id],
            supply: income.supply[id],
            thickness: income.thickness[id],
            color: income.color[id],
            weight: income.weight[id],
            pm: income.pm[id],
            pmfull: income.pmfull[id],
            wagon: income.wagon[id],
            prim: income.prim[id],
            prinyal: income.prinyal[id],
            acceptdate: income.acceptdate[id],
            status: income.status[id], // 0 - закрыт 1 - открыт 2 - списан
            passimgname: income.passimgname[id],
            birkaimgname: income.birkaimgname[id],
            discarddate: income.discarddate[id],
        };
        var data = JSON.stringify(roll);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
})

app.post('/getincomedata', _util.checkSession, function(req, res) { //GET INCOME DATA POST REQUEST
    returnIncomeArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(income);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/register', _util.checkSession, function(req, res) { //REGISTER POST REQUEST
    let login = req.body['username'];
    let pass = req.body['password'];
    let salt = bcrypt.genSaltSync(10);
    let passtosave = bcrypt.hashSync(pass, salt);

    let lastname = req.body['lastname'];
    let surname = req.body['surname'];
    let name = lastname + ' ' + surname;
    let role = req.body['role'];
    connection.query(`INSERT INTO users(username,password,lastname,role,uniqid) VALUES(?,?,?,?,?)`, [login, passtosave, name, role, salt], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    res.redirect('/home');
});

app.post('/getorderdata', function(req, res) { //GET ORDER DATA POST REQUEST
    returnOrderArray().catch(error => { throw createError(500, 'Internal Server Error') }).then((() => {
        var data = JSON.stringify(order);
        res.send(data);
    })).catch(error => createError(500, 'Internal Server Error'));
});

app.post('/changestatusready/:id', _util.checkSession, function(request, response) { //CHANGE STATUS TO READY POST REQUEST
    let raw = request.body['rolls'];
    let necond = request.body['necond'];
    let used = request.body['used'];
    let raws = raw.split(',');
    let neconds = necond.split(',');
    let useds = used.split(',');
    let pm2 = request.body['pm2'];
    let tlen = 0;
    let i = 0;
    var date = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    tlen += pm2;
    i = 0;
    while (i < raws.length) {
        connection.query(`UPDATE rullon SET orders = concat(orders,?),pm = (pm - (?+?)),used = concat(used,?),necond = concat(necond,?) WHERE id = ?`, [request.params.id + ",", neconds[i], useds[i], useds[i] + ",", neconds[i] + ",", raws[i]], function(error) {
            if (error) _err.mysqlErrorHandler(error);
        });
        i++;
    }
    connection.query(`UPDATE orders SET status = 2,roll = concat(roll,?),finishdate = ? WHERE id = ?`, [raw, date, request.params.id], function(error, results) {
        if (error) _err.mysqlErrorHandler(error);
    });
    response.redirect('/order_page/' + request.params.id);
});


////////////
//PROMISES//
////////////

const returnMiscArray = () => {
    var i = 0;
    var id = 0;
    misc = _util.toNull(misc);

    const promise = new Promise((resolve, reject) => {
        while (Number(order.id[id]) != Number(orderid)) id++;
        connection.query(`SELECT * FROM misc WHERE uniqid = ?`, [order.uniqid[id]], function(error, results) {
            while (i < results.length) {
                misc.uniqid[i] = results[i].uniqid;
                misc.id[i] = results[i].id;
                misc.count[i] = results[i].count;
                misc.cost[i] = results[i].cost;
                misc.product[i] = results[i].product;
                misc.fcost[i] = results[i].fcost;
                i++;
            }
            resolve(misc);
        });
    });
    return (promise);
}

const returnIncomeArray = () => {
    var i = 0;
    income = _util.toNull(income);
    const promise = new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM rullon`, function(error, results) {
            while (i < results.length) {
                income.id[i] = results[i].id;
                income.supply[i] = results[i].supply;
                income.thickness[i] = results[i].thickness;
                income.color[i] = results[i].color;
                income.weight[i] = results[i].weight;
                income.pm[i] = results[i].pm;
                income.pmfull[i] = results[i].pmfull;
                income.wagon[i] = results[i].wagon;
                income.prim[i] = results[i].prim;
                income.prinyal[i] = results[i].prinyal;
                income.acceptdate[i] = results[i].acceptdate;
                income.status[i] = results[i].status;
                income.passimgname[i] = results[i].passimgname;
                income.birkaimgname[i] = results[i].birkaimgname;
                income.discarddate[i] = results[i].discarddate;
                i++;
            }
            resolve(income);
        });
    });
    return (promise);
}

const returnPositionsArray = () => {
    var i = 0;
    var id = 0;
    positions = _util.toNull(positions);

    const promise = new Promise((resolve, reject) => {
        while (Number(order.id[id]) != Number(orderid)) id++;
        connection.query(`SELECT * FROM positions WHERE uniqid = ?`, [order.uniqid[id]], function(error, results) {
            while (i < results.length) {
                positions.uniqid[i] = results[i].uniqid;
                positions.id[i] = results[i].id;
                positions.length[i] = results[i].len;
                positions.sqr[i] = results[i].sqr;
                positions.cost[i] = results[i].cost;
                positions.count[i] = results[i].count;
                positions.costpm[i] = results[i].costpm;
                i++;
            }
            resolve(positions);
        });
    });

    return (promise);
}

const returnNecondArray = () => {
    var i = 0;
    var id = 0;
    necond = _util.toNull(necond);
    const promise = new Promise((resolve, reject) => {
        while (Number(order.id[id]) != Number(orderid)) id++; //  Возможна ошибка - переделать
        connection.query(`SELECT * FROM necond WHERE uniqid = ?`, [order.uniqid[id]], function(error, results) {
            while (i < results.length) {
                necond.uniqid[i] = results[i].uniqid;
                necond.id[i] = results[i].id;
                necond.type[i] = results[i].type;
                necond.sqr[i] = results[i].pm;
                necond.cost[i] = results[i].cost;
                necond.fcost[i] = results[i].fcost;
                i++;
            }

            resolve(necond);
        });
    });

    return (promise);
}

const returnPipeArray = () => {
    var i = 0;
    var id = 0;
    pipe = _util.toNull(pipe);
    const promise = new Promise((resolve, reject) => {
        while (Number(order.id[id]) != Number(orderid)) id++;
        connection.query(`SELECT * FROM pipe WHERE uniqid = ?`, [order.uniqid[id]], function(error, results) {
            while (i < results.length) {
                pipe.uniqid[i] = results[i].uniqid;
                pipe.id[i] = results[i].id;
                pipe.type[i] = results[i].type;
                pipe.pm[i] = results[i].pm;
                pipe.cost[i] = results[i].cost;
                pipe.fcost[i] = results[i].fcost;
                i++;
            }
            resolve(pipe);
        });
    });

    return (promise);
}

const returnOrderArray = () => {
    var i = 0;

    const promise = new Promise((resolve, reject) => {
        connection.query('SELECT * FROM orders', function(error, results) {
            while (i < results.length) {
                order.id[i] = results[i].id;
                order.wavetype[i] = results[i].wavetype;
                order.color[i] = results[i].color;
                order.thick[i] = results[i].thick;
                order.fthick[i] = results[i].fthick;
                order.readydate[i] = results[i].readydate;
                order.createdate[i] = results[i].createdate;
                order.uniqid[i] = results[i].uniqid;
                order.manager[i] = results[i].manager;
                order.client[i] = results[i].client;
                order.status[i] = results[i].status;
                order.finallen[i] = results[i].finallen;
                order.finalcost[i] = results[i].finalcost;
                order.finalcount[i] = results[i].finalcount;
                order.finalsqr[i] = results[i].finalsqr;
                order.operator[i] = results[i].operator;
                order.roll[i] = results[i].roll;
                order.misc[i] = results[i].misc;
                order.payment[i] = results[i].payment;
                order.phonenumber[i] = results[i].phonenumber;
                order.cost[i] = results[i].cost;
                order.departdate[i] = results[i].departdate;
                order.productiondate[i] = results[i].productiondate;
                order.finishdate[i] = results[i].finishdate;
                i++;
            }
            resolve(order);
        });
    });
    return promise;
}

////////////////
///PROTOTYPES///
////////////////

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

///////////////////
///ERROR HANDLER///
///////////////////

app.post('/clienterror', (req, res) => {
    _err.logClientErrors(req.body.status, req.body.message, req.session.username);
    res.end();
});

app.use(function(req, res, next) {
    res.status(404).render('notfound.ejs', { title: "Sorry, page not found" });
});

app.use((error, req, res, next) => {
    if (res.headersSent) {
        return next(error)
    }
    error.status = error.status || 500;
    res.status(error.status);
    res.send(error.message);
    let currentdate = new Date().addHours(3).toISOString().slice(0, 19).replace('T', ' ');
    let output = `Error status: ${error.status}   With message: ${error.message}      Date: ${currentdate}\n ${error.stack}\n\n`;
    let line = '/*------------------------------------------------------------------------------------------------------------*/\n\n';
    console.log(output + line);
    fs.appendFile('ERRORS/servererror.log', output + line, function(error) {
        if (error) throw error;

    });

    res.end();
});

// const httpServer = http.createServer(app);
// const httpsServer = https.createServer(credentials, app);

// // httpServer.listen(80, () => {
//     console.log('HTTP SERVER RUNNING ON PORT 80');
// });

// httpsServer.listen(443, () => {
//     console.log('HTTPS SERVER RUNNING ON PORT 443');

// });

app.listen(3000);