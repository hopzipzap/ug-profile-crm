var _ = require('lodash');

module.exports = {
    toNull: function(input) {
        const result = _.mapValues(input, v => _.isNil(v) ? [] : []);
        return result;
    },

    checkSession: function(request, response, next) {
        if (request.session.username) {
            next();
        } else {
            response.redirect('/');
        }
    },

    getRole: function(role) {
        let ret;
        if (role == 0)
            ret = "Администратор";
        if (role == 1)
            ret = "Менеджер";
        if (role == 2)
            ret = "Кладовщик";
        if (role == 3)
            ret = "Карщик";
        if (role == 4)
            ret = "Оператор";
        if (role == 10)
            ret = "Менеджер производства";
        return (ret);
    },
};